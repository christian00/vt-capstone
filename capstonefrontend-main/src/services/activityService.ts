import Api from '@/services/Api'

export const activityApi = {
  async getActivities(id: string) {
    const url = 'activities?course_id=' + id        
    const response = await Api().get(url)
    return response
  },
  async getSessions(id: string) {
    const url = 'activities/sessions?course_id=' + id        
    const response = await Api().get(url)
    return response
  },
}