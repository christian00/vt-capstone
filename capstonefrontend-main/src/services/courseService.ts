import Api from '@/services/Api'

export const courseApi = {
  async getCourses(val: string, isName: boolean) {
    if(!val){ // if no name/email assignmed
     val = 'hychien2000@gmail.com'
     isName = false
    }
    
    let url = 'courses?instructor_email=' + val
    if(isName){
      url = 'courses?instructor_name=' + val
    }
    const response = await Api().get(url)
    console.log(response)
    return response
  },
  async getIps(id: string) {
    const url = 'courses/ips?course_id=' + id        
    const response = await Api().get(url)
    return response
  },
  async getMLResults(id: string) {
    const url = 'courses/ml?course_id=' + id        
    const response = await Api().get(url)
    return response
  },
}