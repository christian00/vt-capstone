import Api from '@/services/Api'

export const assignmentApi = {
  async getAssignments(id: string) {
    const url = 'assignments?course_id=' + id        
    const response = await Api().get(url)
    return response
  },
  async getSubmissions(course_id: String, id: String) {
    const url = 'assignments/submissions?course_id='+course_id + '&assignment_id=' + id        
    const response = await Api().get(url)
    return response
  },
}