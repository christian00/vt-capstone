import Api from '@/services/Api'

export const quizApi = {
  async getQuizzes(id: string) {
    const url = 'quizzes?course_id=' + id        
    const response = await Api().get(url)
    return response
  },
  async getDuration(id: number) {
    const url = 'quizzes/duration?quiz_id=' + id        
    const response = await Api().get(url)
    return response
  },
}