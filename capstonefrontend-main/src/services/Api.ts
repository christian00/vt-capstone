import axios from 'axios'
import serverBaseUrl from '@/config'

export default () => {
    return axios.create({
        baseURL: `${serverBaseUrl}/v1/api/`,
    })
}
