export interface introChart {
  name: string;
  value: string;
  introduction: string;
  imgName: string;
}

export const introCharts: introChart[] = [
  {
    name: "Geolocation",
    value: "geolocation",
    introduction: "Users' geolocation distribution, location is inferred by user IP",
    imgName: "1",
  },
  {
    name: "Activity",
    value: "activity",
    introduction: "Login activity distribution: count active user sessions per day",
    imgName: "2",
  },
  {
    name: "Session",
    value: "session",
    introduction: "User's accumulative session time, find relationship between time spent on course content and academic performance",
    imgName: "3",
  },
  /*
  {
    name: "Prediction",
    value: "prediction",
    introduction: "Academic performance classification: find relationship between time spent on course content and academic performance",
    imgName: "3",
  },*/{
    name: "Submission",
    value: "submission",
    introduction: "Submission time distribution: understand difficulty of assignments",
    imgName: "4",
  },{
    name: "Duration",
    value: "duration",
    introduction: "How long do the students spend on the quiz: understand difficulty of quizzes",
    imgName: "5",
  },{
    name: "Other",
    value: "other",
    introduction: "More charts could be designed...",
    imgName: "6",
  },
]


