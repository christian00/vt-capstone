export interface Quiz {
  name: string;
  value: string;
  start: string;
  end: string;
}

export const fakeQuizzes: Quiz[] = [
  {
    name: "Quiz1",
    value: "quiz1",
    start: "2022-05-03",
    end: "2022-05-10",
  },
  {
    name: "Midterm",
    value: "midterm",
    start: "2022-06-05",
    end: "2022-06-05",
  },
  {
    name: "Quiz2",
    value: "quiz2",
    start: "2022-07-01",
    end: "2022-07-08",
  },{
    name: "Final",
    value: "final",
    start: "2022-08-10",
    end: "2022-08-10",
  },
]


