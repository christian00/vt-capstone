export interface category {
  name: string;
  value: string;
}

export const statisticCategory: category[] = [
  {
    name: "Geolocation", // user ip geo locations of a course
    value: "geolocation",
  },
  {
    name: "Activity", // user login activity time of a course
    value: "activity",
  },
]
export const mlCategory: category[] = [
  {
    name: "Academic Performance",
    value: "ml",
  },
]


