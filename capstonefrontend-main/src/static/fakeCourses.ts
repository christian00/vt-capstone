export interface Course {
  name: string;
  value: string;
  start: string;
  end: string;
}

export const fakeCourses: Course[] = [
  {
    name: "CS101",
    value: "cs101",
    start: "2022-05-03",
    end: "2022-09-30",
  },
  {
    name: "CS102", 
    value: "cs102",
    start: "2022-05-03",
    end: "2022-09-30",
  },
  {
    name: "ECE101",
    value: "ece101",
    start: "2023-01-13",
    end: "2023-05-03",
  },
]


