export interface Assignment {
  name: string;
  value: string;
  start: string;
  end: string;
}

export const fakeAssignments: Assignment[] = [
  {
    name: "Assignment1",
    value: "assignment1",
    start: "2022-05-03",
    end: "2022-05-10",
  },
  {
    name: "Assignment2",
    value: "assignment2",
    start: "2022-06-01",
    end: "2022-06-15",
  },
  {
    name: "Assignment3",
    value: "assignment3",
    start: "2022-08-16",
    end: "2022-09-15",
  },
]


