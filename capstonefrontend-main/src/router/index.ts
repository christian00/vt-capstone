import { createRouter, createWebHistory, type NavigationGuardNext, type RouteLocationNormalized } from 'vue-router'
import { useAuthStore } from '@/stores/authStore'
import { useCourseStore } from '@/stores/courseStore'
import { courseApi } from '@/services/courseService'

import HomeView from '@/views/HomeView.vue'
import LoginView from '@/views/LoginView.vue'
import Introduction from '@/components/Introduction.vue'
import PageNotFound from '@/components/PageNotFound.vue'
// table
import Courses from '@/components/table/Courses.vue'
import Assignments from '@/components/table/Assignments.vue'
import Quizzes from '@/components/table/Quizzes.vue'
// routerview
import CourseRouter from '@/components/routerview/CourseRouter.vue'
import AssignmentRouter from '@/components/routerview/AssignmentRouter.vue'
import QuizRouter from '@/components/routerview/QuizRouter.vue'
// charts
import GeoLocation from '@/components/statistics/GeoLocation.vue'
import Activity from '@/components/statistics/Activity.vue'
import Session from '@/components/statistics/Session.vue'
import Submission from '@/components/statistics/Submission.vue'
import Duration from '@/components/statistics/Duration.vue'
import Classification from '@/components/ml/Classification.vue'

// lazy-loading chart component
const loginGuard = (to: RouteLocationNormalized, from: RouteLocationNormalized, next: NavigationGuardNext) => {
  const authStore = useAuthStore()
  console.log(authStore.getStatus)
  if(authStore.getStatus.isLogin){
    next()
  }else{
    next('/login')
  }
};

const edxGaurd = (to: RouteLocationNormalized, from: RouteLocationNormalized, next: NavigationGuardNext) => {
  const authStore = useAuthStore()
  const courseStore = useCourseStore()

  if(to.query.email){
    courseApi.getCourses(to.query.email.toString(), false).then((success: any) => {
      console.log(success)
      authStore.setStatus( success.data.username, success.data.email)
      courseStore.setCourses(success.data.courses);
      router.push({ name: 'home' })
    }).catch((fail: any) => {
      console.log('Can NOT find such user, login failed', fail);
      next('/login')
    })
  }else{
    next('/login')
  }
};

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      redirect: '/course',
      component: HomeView,
      beforeEnter(to, from, next){
        loginGuard(to, from, next)
      },
      children: [{
        path: 'introduction',
        component: Introduction, // charts preview or dashboard
      },{
        path: 'course', // show courses table 
        name: 'course',
        component: Courses,
      },{
        path: 'course/:cid',
        name: 'course-router',
        component: CourseRouter,
        redirect: to =>{
          return { path: '/course/'+to.params.cid+'/geolocation', params: { cid: to.params.cid } }
        },
        children: [{
          path: 'geolocation', // user ip geo locations of a course
          name: 'geolocation',
          component: GeoLocation,
        },{
          path: 'activity',
          name: 'activity',
          component: Activity,
        },{
          path: 'session',
          name: 'session',
          component: Session,
        },{
          path: 'performance',
          name: 'performance',
          component: Classification,
        }]  
      },{
        path: 'course/:cid/assignment', // show all assignments table 
        name: 'assignment',
        component: Assignments,
      },{
        path: 'course/:cid/assignment/:aid', 
        name: 'assignment-router',
        component: AssignmentRouter,
        redirect: to =>{
          return {
            path: '/course/'+to.params.cid+'/assignment/'+to.params.aid+'/submission',
            params: { cid: to.params.cid, aid: to.params.aid }
          }
        }, 
        children: [{
          path: 'submission', // show assignment submission time distribution
          name: 'submission',
          component: Submission,
        }] 
      },{
        path: 'course/:cid/quiz', // show all quiz table 
        name: 'quiz',
        component: Quizzes,
      },{
        path: 'course/:cid/quiz/:qid', 
        name: 'quiz-router',
        component: QuizRouter,
        redirect: to =>{
          return {
            path: '/course/'+to.params.cid+'/quiz/'+to.params.qid+'/duration',
            params: { cid: to.params.cid, qid: to.params.qid }
          }
        }, 
        children: [{
          path: 'duration', // show quiz time spent distribution 
          name: 'duration',
          component: Duration,
        }] 
      }]  
    },{
      path: '/login',
      name: 'login',
      component: LoginView
    },{
      path: '/loginEdX',
      name: 'login-edx',
      component: LoginView,
      beforeEnter(to, from, next){
        edxGaurd(to, from, next)
      },
    },{
      path: "/:pathMatch(.*)*",
      name: "404",
      component: PageNotFound,
    }
  ]
})

export default router
