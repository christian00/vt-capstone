import { defineStore } from 'pinia'

export const useCourseStore = defineStore('courseStore', {
  state: () => ({
    courses: [] as Course[],
    currentCourse: {
      mysql_id: "",
      mongo_id: "",
      name: "",
      value: "",
      start: "",
      end: "",
    } as Course,
  }),
  getters: {
    getCourses: (state) => state.courses,
    getCurrentCourse: (state) => state.currentCourse,
  },
  actions: {
    setCourses(data: Course[]){
      this.courses = data
    },
    clearCourses(){
      this.courses = []
    },

    setCurrentCourse(data: Course){
      this.currentCourse = data
    },
    clearCurrentCourse(){
      this.currentCourse = {
        mysql_id: "",
        mongo_id: "",
        name: "",
        value: "",
        start: "",
        end: "",
      }
    }
  }
})

export interface Course {
  mysql_id: string,
  mongo_id: string,
  name: string;
  value: string;
  start: string;
  end: string;
}