import { defineStore } from 'pinia'

export const useAssignmentStore = defineStore('assignmentStore', {
  state: () => ({
    assignments: [] as Assignment[],
    currentAssignment: {
      id: "",
      name: "",
      value: "",
      start: "",
      end: "",
      mongo_course_id: ""
    } as Assignment,
  }),
  getters: {
    getAssignments: (state) => state.assignments,
    getCurrentAssignment: (state) => state.currentAssignment,
  },
  actions: {
    setAssignments(data: Assignment[]){
      this.assignments = data
    },
    clearCourses(){
      this.assignments = []
    },

    setCurrentAssignment(data: Assignment){
      this.currentAssignment = data
    },
    clearCurrentAssignment(){
      this.currentAssignment = {
        id: "",
        name: "",
        value: "",
        start: "",
        end: "",
        mongo_course_id: ""
      }
    }
  }
})

export interface Assignment {
  id: string;
  name: string;
  value: string;
  start: string;
  end: string;
  mongo_course_id: string;
}