import { defineStore } from 'pinia'

export const useAuthStore = defineStore('authStatus', {
  state: () => ({
    user: {
      isLogin: false,
      name: "",
      email: "",
    } as User,
  }),
  getters: {
    getStatus: (state) => state.user,
  },
  actions: {
    setStatus(name: string, email: string){
      this.user.isLogin = true
      this.user.name = name
      this.user.email = email
    },
    setLogout(){
      this.user.isLogin = false
      this.user.name = ""
      this.user.email = ""
    }
  }
})

export interface User {
  isLogin: boolean
  name: string
  email: string
}