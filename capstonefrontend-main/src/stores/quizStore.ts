import { defineStore } from 'pinia'

export const useQuizStore = defineStore('quizStore', {
  state: () => ({
    quizzes: [] as Quiz[],
    currentQuiz: {
      id: -1,
      name: "",
      value: "",
      start: "",
      end: "",
      limit: -1,
      mysql_course_id: "",
      mongo_course_id: ""
    } as Quiz,
  }),
  getters: {
    getQuizzes: (state) => state.quizzes,
    getCurrentQuiz: (state) => state.currentQuiz,
  },
  actions: {
    setQuizzes(data: Quiz[]){
      this.quizzes = data
    },
    clearCourses(){
      this.quizzes = []
    },

    setCurrentQuiz(data: Quiz){
      this.currentQuiz = data
    },
    clearCurrentQuiz(){
      this.currentQuiz = {
        id: -1,
        name: "",
        value: "",
        start: "",
        end: "",
        limit: -1,
        mysql_course_id: "",
        mongo_course_id: ""
      }
    }
  }
})

export interface Quiz {
  id: number;
  name: string;
  value: string;
  start: string;
  end: string;
  limit: number; // unit: mins
  mysql_course_id: string,
  mongo_course_id: string
}