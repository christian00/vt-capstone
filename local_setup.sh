#!/bin/bash

VENV_DIR="myvenv"

check_python_version() {
    # Check the installed Python version
    if command -v python3 &>/dev/null; then
        # Get the Python version number
        py_ver=$(python3 -c 'import platform; print(platform.python_version())')
        echo "Found Python $py_ver."
    else
        echo "Python 3 is not installed."
        exit 1
    fi
}

# Create a virtual environment using the venv module
create_virtual_env() {
    if [ -d "$VENV_DIR" ]; then
        echo "$VENV_DIR already exists."
    else
        echo $VENV_DIR
        python3 -m venv "$VENV_DIR"
        echo "Created virtual environment $venv_dir."
    fi
}

# Function to check if the current machine is an M1 Mac
is_m1_mac() {
    # Get the CPU architecture
    arch=$(uname -m)

    # Check if the architecture is "arm64"
    if [ "$arch" = "arm64" ]; then
        return 0 # true
    else
        return 1 # false
    fi
}

check_python_version
create_virtual_env
source $VENV_DIR/bin/activate 
# install tutor
pip install "tutor[full]"

if is_m1_mac; then
    echo "Is M1 Mac"
    # change mysql docker image to mariadb
    tutor config save --set DOCKER_IMAGE_MYSQL=mariadb:10.4
fi



tutor config save --set PLATFORM_NAME=team1-openedx
tutor config save --set LMS_HOST="local.overhang.io"
tutor config save --set CMS_HOST="studio.local.overhang.io"
tutor config save --set DOCKER_REGISTRY="docker.io/"
tutor config save --set DOCKER_IMAGE_OPENEDX="{{ DOCKER_REGISTRY }}overhangio/openedx:{{ TUTOR_VERSION }}"

sudo sh add_dns_records.sh

tutor local launch
