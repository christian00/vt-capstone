# VT Capstone
## Team Members
* Christian Ross (christian00@vt.edu)
* Grayson Childs (gtc@vt.edu)
* Hsin-Yu(Jessica) Chien (hsinyu@vt.edu)
* Robert Benish (rabenish@vt.edu)
* Yi-Han(Hannah) Chen (yihanchen@vt.edu)

## Name
Open edX Project A - VT Capstone Team 1

## Description
The purpose of this system is to design and develop an Open edX course with content pulled from an existing Canvas course. The end product will demonstrate the capabilities of using Open edX which will allow the client to determine whether using Open edX is right for their needs. Overall, the addition of functionality to the Open edX platform will be the focus of our development. Our team has decided to implement both a data visualization page for student data to assess how they are performing in the course as well as a course conversion feature to allow for easy import of course material to the edX platform.

## Demo Videos
* [Data visualization web app](/video/demo/data%20visualization.mov)
* [Fetching edX course content](/video/demo/fetching%20openedx%20course%20content.mov)
* [Import course from Canvas](/video/demo/converter-intergration.mp4)

## Code Walkthrough Videos
* [runtime course converter](/video/code%20walkthrough/converter_runtime.mp4)
* [Data visualization web app frontend](/video/code%20walkthrough/frontend_walkthrough.mov)
* [Data visualization web app backend](/video/code%20walkthrough/backend_walkthrough.mov)

## Getting started

```
make local-setup
```

## Prerequisite
* Python 3
* Docker

reference: 
* [Installing Tutor](https://docs.tutor.overhang.io/install.html)
* [Running Tutor on ARM-based systems](https://docs.tutor.overhang.io/tutorials/arm64.html)

### Subcomponents
* Data Visualization Frontend
* Data Processing Backend
* Course Converter: Canvas Exported File to Open edX Import File
----------------

***

## Authors and acknowledgment
* Christian Ross (christian00@vt.edu)
* Grayson Childs (gtc@vt.edu)
* Hsin-Yu(Jessica) Chien (hsinyu@vt.edu)
* Robert Benish (rabenish@vt.edu)
* Yi-Han(Hannah) Chen (yihanchen@vt.edu)

## License
For open source projects, say how it is licensed.
