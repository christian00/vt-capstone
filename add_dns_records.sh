# Check if the script is run with sudo
if [ "$EUID" -ne 0 ]; then
    echo "Please run this script as root or with sudo."
    exit 1
fi

if grep -q "127.0.0.1    local.overhang.io" /etc/hosts  && 
    grep -q "127.0.0.1    studio.local.overhang.io" /etc/hosts; then
    echo "The required records already exist in /etc/hosts. Exiting..."
    exit 1
fi
# Add DNS records to the /etc/hosts file
echo "Adding DNS records to /etc/hosts..."

# Example DNS records
sudo echo "127.0.0.1    local.overhang.io" >> /etc/hosts
sudo echo "127.0.0.1    studio.local.overhang.io" >> /etc/hosts
# Output a message indicating that the DNS records have been added
echo "DNS records added to /etc/hosts."
