from typing import List

from model.canvas.course import CanvasCourse
from model.canvas.module import Module
from model.canvas.item import ContentType, Item
from model.canvas.assignment import Assignment

from model.openedx.course import OpenEdxCourse
from model.openedx.chapter import Chapter
from model.openedx.sequential import Sequential
from model.openedx.vertical import Vertical
from model.openedx.component import ComponentType, Component, VideoComponent, OpenAssessmentComponent, ProblemComponent, HTMLComponent

class CourseConverter:
    def __init__(self, canvasCourse: CanvasCourse, openEdxCourse: OpenEdxCourse):
        self.canvasCourse = canvasCourse
        self.openEdxCourse = openEdxCourse

    """
    # convert canvasCourse.canvasModules to openEdxCourse.edxChapters
    """
    def convert_modules_to_chapters(self):
        for index in self.canvasCourse.canvasModules:
            module = self.canvasCourse.canvasModules[index]
            self.convert_single_module(index, module)

    def convert_single_module(self, index: int, module: Module):
        chapter = Chapter(display_name=module.get_title())
        chapter.set_sequentials(self.convert_itmes_to_sequentials(module.get_items()))
        self.openEdxCourse.edxChapters[index] = chapter   
        
    def convert_itmes_to_sequentials(self, items: List[Item]) -> List[Sequential]:
        sequentials = []
        for item in items:
            sequentials.append(self.convert_single_item(item))
        return sequentials

    def convert_single_item(self, item: Item)-> Sequential:
        # (1) create a single sequential containing one vertical containing one <component>
        sequential = Sequential(display_name=item.get_title())
        vertical = Vertical(display_name=item.get_title())
        
        # (2) check item type, create corresponding component and append componetn to vertical
        vertical.append_component_to_components(self.create_component(item))

        sequential.append_vertical_to_verticals(vertical)
        return sequential

    def create_component(self, item: Item)-> Component:
        component = None
        if(item.get_content_type() == ContentType['Assignment']):
            isOpenassessment = True #@TODO
            if(isOpenassessment):
                component = OpenAssessmentComponent(component_type=ComponentType['openassessment'], display_name=item.get_title())
                self.convert_assignment_item(item, component)
            else:
                component = ProblemComponent(component_type=ComponentType['problem'], display_name=item.get_title())
                self.convert_assignment_item_to_quiz(item, component)
        elif(item.get_content_type() == ContentType['ContextModuleSubHeader']):
            component = HTMLComponent(component_type=ComponentType['html'], display_name=item.get_title())
            self.convert_header_item(item, component)
        elif(item.get_content_type() == ContentType['ExternalUrl']):
            component = HTMLComponent(component_type=ComponentType['html'], display_name=item.get_title())
            self.convert_url_item(item, component)
        elif(item.get_content_type() == ContentType['WikiPage']):
            component = HTMLComponent(component_type=ComponentType['html'], display_name=item.get_title())
            self.convert_wiki_item(item, component)
        else:
            None
        return component

    def convert_assignment_item(self, item: Item, component: OpenAssessmentComponent):
        assignment = item.get_assignment()
        text = str(assignment.get_intro_body())
        component.set_html_content(text)

    def convert_assignment_item_to_quiz(self, item: Item, component: OpenAssessmentComponent):
        None

    def convert_header_item(self, item: Item, component: HTMLComponent):
        None

    def convert_url_item(self, item: Item, component: HTMLComponent):
        component.set_html_content('<a href="' + item.get_url() + '">' + item.get_title() + '</a>')

    def convert_wiki_item(self, item: Item, component: HTMLComponent):
        body = str(self.canvasCourse.canvasWikiBody[item.get_identifierref()])
        component.set_html_content(body)
