import os
import xml.etree.ElementTree as ET
from bs4 import BeautifulSoup
import copy

from model.canvas.course import CanvasCourse, CanvasDir
from model.canvas.module import Module
from model.canvas.item import Item, ContentType
from model.canvas.resource import Resource, File, Dependency
from model.canvas.assignment import Assignment

class CanvasCourseParser:

    def __init__(self, COURSE_ROOT_DIR_NAME: str="default-canvas-root-dir"):
        self.course: CanvasCourse = None
        self.canvas_dir: CanvasDir = None
        self.COURSE_ROOT_DIR_NAME = COURSE_ROOT_DIR_NAME

    def get_unzipped_directory(self) -> CanvasDir:
        self.canvas_dir = CanvasDir(self.COURSE_ROOT_DIR_NAME)
        self.canvas_dir.unzipParentDir()
        #print("successfully unzip .imscc file.")
        print("successfully unzip .imscc file to:")
        print(self.canvas_dir.canvas_root_dir)

    def set_static_directory(self) -> CanvasDir:
        self.canvas_dir = CanvasDir(self.COURSE_ROOT_DIR_NAME)
        self.canvas_dir.parent_dir = "example"
        self.canvas_dir.canvas_root_dir = "example/"+ self.COURSE_ROOT_DIR_NAME
        print("parse canvas from static folder:")
        print(self.canvas_dir.canvas_root_dir)
    
    def create_course(self) -> None:
        self.course = CanvasCourse()
        
    """
    # parse /course_settings folder under parent directory
    """
    # /course_settings
    def parse_course_settings_folder(self, printLog: bool = False):
      all_entries = os.listdir(self.canvas_dir.canvas_root_dir + '/course_settings')
      for entry in all_entries:
        if(entry == 'assignment_groups.xml'):
          self.parse_assignment_groups_xml(self.canvas_dir.canvas_root_dir + '/course_settings/' + entry, printLog)
        elif(entry == 'context.xml'):
          self.parse_context_xml(self.canvas_dir.canvas_root_dir + '/course_settings/' + entry, printLog)
        elif(entry == 'course_settings.xml'):
          self.parse_course_settings_xml(self.canvas_dir.canvas_root_dir + '/course_settings/' + entry, printLog)
        elif(entry == 'files_meta.xml'):
          self.parse_files_meta(self.canvas_dir.canvas_root_dir + '/course_settings/' + entry, printLog)
        elif(entry == 'module_meta.xml'):
          self.parse_module_meta(self.canvas_dir.canvas_root_dir + '/course_settings/' + entry, printLog)
        else:
          # print("skip parsing:", entry)
          continue

    # /course_settings/assignment_groups.xml
    def parse_assignment_groups_xml(self, filePath:str, printLog: bool = False):
      tree = ET.parse(filePath)
      root = tree.getroot()
      if(printLog):
        print('====== /course_settings/assignment_groups.xml ========')
      for element in root:
        if(element.tag.endswith("assignmentGroup")):
          if(printLog):
            print('assignmentGroup')
            print('identifier:', element.attrib['identifier'])
          for subElement in element:
            if(subElement.tag.endswith("title")):
              if(printLog):
                print('title:', subElement.text)
            elif(subElement.tag.endswith("group_weight")):
              if(printLog):
                print('group_weight:', subElement.text)
      if(printLog):
        print('====== parse end ========\n\n')

    # /course_settings/context.xml
    def parse_context_xml(self, filePath:str, printLog: bool = False):
      tree = ET.parse(filePath)
      root = tree.getroot() # "context_info"
      if(printLog):
        print('====== /course_settings/context.xml ========')
      for element in root:
        if(element.tag.endswith("course_name")):
          if(printLog):
            print('course_name:', element.text)
          self.course.course_name = element.text
        elif(element.tag.endswith("root_account_name")):
          if(printLog):
            print('root_account_name:', element.text)
      if(printLog):
        print('====== parse end ========\n\n')

    # /course_settings/course_settings.xml
    def parse_course_settings_xml(self, filePath:str, printLog: bool = False):
      tree = ET.parse(filePath)
      root = tree.getroot() # "course"
      self.course.course_setttings_identifier = root.attrib['identifier']
      if(printLog):
        print('====== /course_settings/course_settings.xml ========')
      for element in root:
        if(element.tag.endswith("title")):
          if(printLog):
            print('title:', element.text)
        elif(element.tag.endswith("course_code")):
          if(printLog):
            print('course_code:', element.text)
        elif(element.tag.endswith("is_public")):
          if(printLog):
            print('is_public:', element.text)
        elif(element.tag.endswith("self_enrollment")):
          if(printLog):
            print('self_enrollment:', element.text)
      if(printLog):
        print('====== parse end ========\n\n')

    # /course_settings/files_meta.xml
    def parse_files_meta(self, filePath:str, printLog: bool = False):
      tree = ET.parse(filePath)
      root = tree.getroot() # "fileMeta"
      if(printLog):
        print('====== /course_settings/files_meta.xml ========')
      for element in root:
        if(element.tag.endswith("folders")):
          if(printLog):
            print("folders:")
          for subElement in element:
            if(subElement.tag.endswith("folder")):
              if(printLog):
                print('path:', subElement.attrib['path'])
                print('static path should be:', self.canvas_dir.canvas_root_dir+"/web_resources/"+subElement.attrib['path'])
        if(element.tag.endswith("files")):
          if(printLog):
            print("files:")
          for subElement in element:
            if(subElement.tag.endswith("file")):
              if(printLog):
                print('identifier:', subElement.attrib['identifier'])
      if(printLog):
        print('====== parse end ========\n\n')

    # /course_settings/module_meta.xml
    def parse_module_meta(self, filePath:str, printLog: bool = False):
      tree = ET.parse(filePath)
      root = tree.getroot() # "modules"
      if(printLog):
        print('====== /course_settings/module_meta.xml ========')
      for module in root:
        if(module.tag.endswith("module")):
          if(printLog):
            print("module:")
            print('identifier:', module.attrib['identifier'])
          moduleId = 0
          tmpCanvasModule = Module(identifier= module.attrib['identifier'])
          
          for element in module:
            if(element.tag.endswith("title")):
              if(printLog):
                print('title:', element.text)
              tmpCanvasModule.set_title(element.text)
            elif(element.tag.endswith("position")):
              if(printLog):
                print('position:', element.text)
              moduleId = int(element.text)
              tmpCanvasModule.set_position(element.text)
            elif(element.tag.endswith("items")):
              if(printLog):
                print("\nitems:")
              for item in element:
                if(item.tag.endswith("item")):
                  tmpCanvasItem = Item(identifier= item.attrib['identifier'])
                  self.parse_item(tmpCanvasItem, item)
                  tmpCanvasModule.append_item_to_items(tmpCanvasItem)
          self.course.canvasModules[moduleId] = tmpCanvasModule
      if(printLog):
        print('=========================')
        print(len(self.course.canvasModules), "modules are parsed")
        print('====== parse end ========\n\n')
    
    def parse_item(self, itemObj, item_element, printLog: bool = False) -> Item:
      if(printLog):
        print('identifier:', item_element.attrib['identifier'])

      for element in item_element:
        if(element.tag.endswith("content_type")):
          # either ContextModuleSubHeader, WikiPage, Assignment, ExternalUrl
          if(printLog):
            print('content_type:', element.text)
          itemObj.set_content_type(ContentType[element.text])

        elif(element.tag.endswith("title")):
          if(printLog):
            print('title:', element.text)
          itemObj.set_title(element.text)

        elif(element.tag.endswith("identifierref")):
          if(printLog):
            print('identifierref:', element.text)
          itemObj.set_identifierref(element.text)
        
        elif(element.tag.endswith("url")):
          if(printLog):
            print('url:', element.text)
          itemObj.set_url(element.text)
      if(printLog):
        print("\n")

    """
    # parse /imsmanifest.xml under parent directory
    """
    # /imsmanifest.xml
    def parse_imsmanifest_xml(self, printLog: bool = False):
      tree = ET.parse(self.canvas_dir.canvas_root_dir + "/imsmanifest.xml")
      root = tree.getroot()

      if(printLog):
        print('====== /imsmanifest.xml ========')
      for element in root:
        if(element.tag.endswith("metadata")):
          if(printLog):
            print("can skip meta data")
        elif(element.tag.endswith("organizations")):
          if(printLog):
            print("can skip organizations (have been parsed in module_meta.xml)")
        elif(element.tag.endswith("resources")):
          if(printLog):
            print("resources:")
          for resource in element:
            if(resource.tag.endswith("resource")):
              if(resource.attrib['identifier'] == self.course.course_settings_identifier):
                # can skip course_settings resource
                continue
              else:
                # print('identifier:', resource.attrib['identifier'])
                tmpResource = Resource(identifier= resource.attrib['identifier'])
                self.parse_imsmanifest_resource(tmpResource, resource)
                self.course.canvasResources[tmpResource.get_identifier()] = tmpResource
                """
                print(tmpResource.get_identifier())
                print(tmpResource.get_type())
                print(tmpResource.get_href())
                print("files,", len(tmpResource.get_files()))
                print("depednecy,", len(tmpResource.get_dependencies()))
                for file in tmpResource.get_files():
                  print("file", file.get_href())
                """

      if(printLog):
        print('====== parse end ========\n\n')

    def parse_imsmanifest_resource(self, resourceObj, resource_element, printLog: bool = False):
      if(resource_element.attrib['type'].startswith('webcontent')):
        resourceObj.set_type(resource_element.attrib['type'])
        resourceObj.set_href(resource_element.attrib['href'])

        # print("webcontent resource")
        # only need to parse web_resources (static files)
        if (resource_element.attrib['href'].startswith('web_resources')):
          # static file under /web_resources
          None
        elif (resource_element.attrib['href'].startswith('wiki_content')):
          # wiki xml file under /wiki_content
          self.extract_html_body_in_wiki(resource_element.attrib['identifier'], resource_element.attrib['href'])
          for subElement in resource_element:
            if(subElement.tag.endswith("file")):
              tmpFile = File(href=subElement.attrib['href'])
              resourceObj.append_file_to_files(tmpFile)
            elif(subElement.tag.endswith("dependency")):
              resourceObj.append_dependency_to_dependencies(Dependency(identifierref=subElement.attrib['identifierref']))
      
      elif(resource_element.attrib['type'].startswith('associatedcontent')):
        for subElement in resource_element:
          if(subElement.tag.endswith("file")):
            tmpFile = File(href=subElement.attrib['href'])
            resourceObj.append_file_to_files(tmpFile)
          elif(subElement.tag.endswith("dependency")):
            resourceObj.append_dependency_to_dependencies(Dependency(identifierref=subElement.attrib['identifierref']))
      
      elif(resource_element.attrib['type'].startswith('ims')):
        # ims resource no href attribute
        # not urgent to parse ims resources 
        for subElement in resource_element:
          if(subElement.tag.endswith("file")):
            tmpFile = File(href=subElement.attrib['href'])
            resourceObj.append_file_to_files(tmpFile)
          elif(subElement.tag.endswith("dependency")):
            resourceObj.append_dependency_to_dependencies(Dependency(identifierref=subElement.attrib['identifierref']))
      else:
        return None
      if(printLog):
        print("\n")

    """
    # extract <body> in /wiki_content/[file]
    """
    # /wiki_content
    def extract_html_body_in_wiki(self, identifier: str, href: str, printLog: bool = False):
      # skip iframe and img in body (href/src might not work)
      filePath = self.canvas_dir.canvas_root_dir + "/" + href
      if(printLog):
        print('====== extract wiki in /'+ href +' ========')

      html = None
      try:
        with open(filePath) as fp:
          html = BeautifulSoup(fp, "html.parser")
      except:
        print("cant open html file, faild to parse wiki in /"+ href +"\n")
        return  
      body = html.body
      body.name = "div" # change body tag to <div>
      self.course.canvasWikiBody[identifier] = body
      if(printLog):
        print('====== parse end ========\n\n')

    """
    # parse /web_resources folder under parent directory
    """
     # /web_resources
    def parse_web_resources_folder(self, printLog: bool = False):
        None

    """
    # parse /[identifier] folders under parent directory
    # each fold is a "item" on Canvas
    """
    # /[identifier]
    def parse_all_identifier_folder(self, printLog: bool = False):
      all_entries = os.listdir(self.canvas_dir.canvas_root_dir)
      for entry in all_entries:
        isFolder = os.path.isdir( os.path.join(self.canvas_dir.canvas_root_dir, entry))
        if(isFolder):
          if(self.course.resourceExists(entry)):
            # print("resource has been parsed", entry)
            self.parse_single_identifier_folder("")
        elif(entry.endswith('.xml')):
          if(self.course.resourceExists(entry)):
            # print("resource has been parsed", entry)
            self.parse_single_identifier_xml(entry[0:-4])
    
    # /[identifier]
    def parse_single_identifier_folder(self, identifier: str, printLog: bool = False):
        None

    """
    # parse /[identifier].xml
    """
    # /[identifier].xml
    # usually announcemnt, no need to convert
    def parse_single_identifier_xml(self, identifier: str, printLog: bool = False):
      None

    """
    # Process all items parsed from /course_settings/module_meta.xml
    # ContentType 1: Assignment
    #   Need to create Assignment obj and parse <body> from course.canvasResources
    # ContentType 2: ContextModuleSubHeader
    #   No need to process, since the item contains only title
    # ContentType 3: ExternalUrl
    #   No need to process, since we already parse url in parse_module_meta(),
    #   however, the url may require auth on Canvas, thus invalid to Open edX users
    # ContentType 4: WikiPage
    #   No need to process, wikiBody has been parsed in parse_imsmanifest_resource()
    """
    def process_items_in_modules(self, printLog: bool = False):
      for moduleNumber in self.course.canvasModules:
        moduleObj = self.course.canvasModules[moduleNumber]
        for item in moduleObj.get_items():
          if(item.get_content_type() == ContentType['Assignment']):
            # print("processed assignment:", item.get_title())
            self.extract_assignment_info_in_identifier_xml(item)
          elif(item.get_content_type() == ContentType['ContextModuleSubHeader']):
            # print("processed subheader:", item.get_title())
            None
          elif(item.get_content_type() == ContentType['ExternalUrl']):
            # print("processed external_url:", item.get_title())
            None
          elif(item.get_content_type() == ContentType['WikiPage']):
            # print("processed wiki_page:", item.get_title())
            None
          else:
            if(printLog):
              print("prcoessing fail: not valid item content_type")
    
    def extract_assignment_info_in_identifier_xml(self, item: Item, printLog: bool = False):
      """
      assignment item must have identifierref, which is also folder name under root dir
      in /item.get_identifierref() must can find two files:
      (1) [assignment-title-slug].html, in which <body >defines assignment description
      (2) assignment_settings.xml, which defines assignment config.
      """
      # print("item", item.get_identifierref())
      if self.course.resourceExists(item.get_identifierref()):
        canvasResource = self.course.canvasResources[item.get_identifierref()]
        tmpAssignment = Assignment(identifier = item.get_identifierref())
        tmpAssignment.set_title(item.get_title())
        
        # print("how many files:", len(canvasResource.get_files()))
        for file in canvasResource.get_files():
          # print(file_subPath.get_href())
          filePath = self.canvas_dir.canvas_root_dir + "/" + file.get_href()
          
          # [assignment-title-slug].html
          if(filePath.endswith("html")):
            html = None
            try:
              with open(filePath) as fp:
                html = BeautifulSoup(fp, "html.parser")
            except:
              print("cant open html file:"+filePath+"\n")
              continue
            
            body = html.body
            body.name = "div" # change body tag to <div>
            tmpAssignment.set_intro_body(body)

          # assignment_settings.xml
          elif(filePath.endswith("xml")):      
            # extract config
            tree = ET.parse(filePath)
            root = tree.getroot() # "assignment"
            for element in root:
              if(element.tag.endswith("due_at")):
                tmpAssignment.set_due_at(element.text)
              elif(element.tag.endswith("assignment_group_identifierref")):
                tmpAssignment.set_group_identifierref(element.text)

        item.set_assignment(tmpAssignment)
      else:
        print("ref_resource NOT FOUND for assignment item")
          
    """
    # Show parsed modules & items
    #      
    """
    def show_parsed_modules_items(self):
      print("==========================================")
      print("Course Name:", self.course.course_name)
      print("==========================================")
      print("Total:", len(self.course.canvasModules), "modules\n")
      for moduleNumber in self.course.canvasModules:
        moduleObj = self.course.canvasModules[moduleNumber]
        print("Module Name:", moduleObj.get_title())
        print("Total:", len(moduleObj.get_items()), "items")
        for item in moduleObj.get_items():
          print("Item Name:", item.get_title())
          # print("Item type:", item.get_content_type())
        print("==========================================")
    
    
