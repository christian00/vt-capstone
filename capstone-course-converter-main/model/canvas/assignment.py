class Assignment:
    def __init__(
        self,
        identifier: str = "",
        title: str = "",
        due_at: str = "",
        group_identifierref: str = "",
        intro_body = None,
    ):
        self.identifier = identifier
        self.title = title
        self.due_at = due_at
        self.group_identifierref = group_identifierref
        self.intro_body = intro_body

    # getters
    def get_identifier(self):
        return self.identifier

    def get_title(self):
        return self.title

    def get_due_at(self):
        return self.due_at

    def get_group_identifierref(self):
        return self.group_identifierref

    def get_intro_body(self):
        return self.intro_body

    # setters
    def set_identifier(self, identifier):
        self.identifier = identifier

    def set_title(self, title):
        self.title = title

    def set_due_at(self, due_at):
        self.due_at = due_at

    def set_group_identifierref(self, group_identifierref):
        self.group_identifierref = group_identifierref

    def set_intro_body(self, intro_body):
        self.intro_body = intro_body
