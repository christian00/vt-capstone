import uuid
from enum import Enum
from model.canvas.assignment import Assignment

"""
Canvas: High level classification (terms used on GUI) -> ContentType in exported xmls

Assignment -> Assignment
Quiz -> Assignment
File -> (UNTRACED), however not urgernt to import
Page -> WikiPage
Discussion -> (UNTRACED), however no meaning to import previos dicussion
Text Header -> ContextModuleSubHeader
External URL -> ExternalUrl
External Tool -> (UNTRACED), however not urgernt to import
"""

class ContentType(Enum):
    Assignment = 1
    ContextModuleSubHeader = 2
    ExternalUrl = 3
    WikiPage = 4

class Item:
    def __init__(
        self,
        identifier: str = uuid.uuid4().hex,
        identifierref: str = "",
        content_type: ContentType = ContentType.WikiPage,
        workflow_state: str = "",
        title: str = "",
        url: str = "",
        position: int = None,
        new_tab: bool = False,
        indent: int = 0,
        link_settings_json: str = "null",
        assignment: Assignment = None,
    ):
        self.identifier = identifier
        self.identifierref = identifierref
        self.content_type = content_type
        self.workflow_state = workflow_state
        self.title = title
        self.url = url
        self.position = position
        self.new_tab = new_tab
        self.indent = indent
        self.link_settings_json = link_settings_json
        self.assignment = assignment

    # getters
    def get_identifier(self):
        return self.identifier

    def get_identifierref(self):
        return self.identifierref

    def get_content_type(self):
        return self.content_type

    def get_workflow_state(self):
        return self.workflow_state

    def get_title(self):
        return self.title

    def get_url(self):
        return self.url

    def get_position(self):
        return self.position

    def get_new_tab(self):
        return self.new_tab

    def get_indent(self):
        return self.indent

    def get_link_settings_json(self):
        return self.link_settings_json

    def get_assignment(self):
        return self.assignment

    # setters
    def set_identifier(self, identifier: str):
        self.identifier = identifier

    def set_identifierref(self, identifierref: str):
        self.identifierref = identifierref

    def set_content_type(self, content_type: ContentType):
        self.content_type = content_type

    def set_workflow_state(self, workflow_state: str):
        self.workflow_state = workflow_state

    def set_title(self, title: str):
        self.title = title

    def set_url(self, url: str):
        self.url = url

    def set_position(self, position: int):
        self.position = position

    def set_new_tab(self, new_tab: bool):
        self.new_tab = new_tab

    def set_indent(self, indent: int):
        self.indent = indent

    def set_link_settings_json(self, link_settings_json: str):
        self.link_settings_json = link_settings_json

    def set_assignment(self, assignment: Assignment):
        self.assignment = assignment
