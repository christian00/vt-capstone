import uuid
from typing import List

class File:
  def __init__(self, href: str):
    self.href = href
  
  def get_href(self):
    return self.href

  def set_href(self, href: str):
    self.href = href

class Dependency:
  def __init__(self, identifierref: str):
    self.identifierref = identifierref
  
  def get_identifierref(self):
    return self.identifierref

  def set_identifierref(self, identifierref: str):
    self.identifierref = identifierref

class Resource:
  def __init__(
    self,
    identifier: str = uuid.uuid4().hex,
  ):

    self.identifier = identifier
    self.type = ""
    self.href = ""
    self.files: List[File] = []
    self.dependencies: List[Dependency] = []

  # getters
  def get_identifier(self):
    return self.identifier

  def get_type(self):
    return self.type

  def get_href(self):
    return self.href

  def get_files(self):
    return self.files
  
  def get_dependencies(self):
    return self.dependencies
  
  # setters
  def set_identifier(self, identifier: str):
    self.identifier = identifier

  def set_type(self, type: str):
    self.type = type

  def set_href(self, href: str):
    self.href = href

  def set_files(self, files: List[File]):
    self.files = files

  def append_file_to_files(self, file: File):
    self.files.append(file)
  
  def set_dependencies(self, dependencies: List[Dependency]):
    self.dependencies = dependencies

  def append_dependency_to_dependencies(self, dependency: Dependency):
    self.dependencies.append(dependency)