import uuid
from typing import List
from model.canvas.item import Item

class Module:
    def __init__(
        self,
        title: str = "",
        identifier: str = uuid.uuid4().hex,
        workflow_state: str = None,
        position: int = None,
        locked: bool = False,
    ):

        self.title = title
        self.identifier = identifier
        self.workflow_state = workflow_state
        self.position = position
        self.locked = locked
        self.items: List[Item] = []

    # getters
    def get_title(self):
        return self.title

    def get_workflow_state(self):
        return self.workflow_state

    def get_identifier(self):
        return self.identifier

    def get_position(self):
        return self.position

    def get_locked(self):
        return self.locked

    def get_items(self):
        return self.items
    
    # setters
    def set_title(self, title: str):
        self.title = title

    def set_workflow_state(self, workflow_state: str):
        self.workflow_state = workflow_state

    def set_identifier(self, identifier: str):
        self.identifier = identifier

    def set_position(self, position: int):
        self.position = position

    def set_locked(self, locked: bool):
        self.locked = locked

    def set_items(self, items: List[Item]):
        self.items = items
    
    def append_item_to_items(self, item: Item):
        self.items.append(item)
