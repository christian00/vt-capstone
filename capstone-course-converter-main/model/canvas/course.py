import os
import zipfile
import xml.etree.ElementTree as ET

from model.canvas.module import Module
from model.canvas.item import Item

class CanvasDir:
  def __init__(self, parsed_dir_name: str):
    self.parent_dir = None
    self.canvas_root_dir = None
    self.parsed_dir_name = parsed_dir_name
  
  def unzipParentDir(self):
    self.unzipFile(self.changeFileExtension(self.detectImsccFile()))

  def detectImsccFile(self):
    cwd = os.getcwd()
    for root, dirs, files in os.walk(cwd):
      for file in files:
        if file.endswith(".imscc"):
          print(".imscc file found:")
          print(os.path.join(root, file))
          self.parent_dir = root
          return os.path.join(root, file)

  def changeFileExtension(self, fileName: str):
    zipFile = fileName[0: -5]+"zip"
    os.rename(fileName, zipFile)
    print("change file extension to zip:")
    print(zipFile)
    return zipFile

  def unzipFile(self, path:str):
    with zipfile.ZipFile(path, 'r') as zip_ref:
      self.canvas_root_dir = os.path.join(self.parent_dir, self.parsed_dir_name)
      zip_ref.extractall(self.canvas_root_dir)
    #print("unzipped canvas exported files, set root directory done")
    print("unzipped canvas exported files, set root directory to:")
    print(self.canvas_root_dir)
        
class CanvasCourse:
  def __init__(self) -> None:
    self.course_name = ""
    # /course_settings/course_settings.xml
    self.course_settings_identifier = "" 

    # dictionary: [module number#] to Module Object
    self.canvasModules = {} 

    # dictionary: [resource identifier] to Resource Object
    self.canvasResources = {} 

    # dictionary: [item identifier] to wiki body ET element (using <div> as tag)
    self.canvasWikiBody= {} 
    
  # check if the entry is a identifier exist in parsed resources
  def resourceExists(self, identifier: str) -> bool:
    return (identifier in self.canvasResources)
