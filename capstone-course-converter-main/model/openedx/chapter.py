import uuid
from typing import List
import xml.etree.ElementTree as ET

from model.openedx.sequential import Sequential

class Chapter:
    def __init__(self, display_name: str = "Section"):
        self.url_name = uuid.uuid4().hex
        self.display_name = display_name
        self.sequentials: List[Sequential] = []

    # getters
    def get_url_name(self):
        return self.url_name
    
    def get_display_name(self):
        return self.display_name

    def get_sequentials(self):
        return self.sequentials

    # setters
    def set_sequentials(self, sequentials: List[Sequential]):
        self.sequentials = sequentials
    
    def append_sequential_to_sequentials(self, sequential: Sequential):
        self.sequentials.append(sequential)

    # methods