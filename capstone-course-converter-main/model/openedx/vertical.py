import os
import uuid
from typing import List
import xml.etree.ElementTree as ET

from model.openedx.component import Component

class Vertical:
    def __init__(self, display_name: str = "Unit"):
        self.url_name = uuid.uuid4().hex
        self.display_name = display_name
        self.filepath = ""
        self.components: List[Component] = []
    
    # getters
    def get_url_name(self):
        return self.url_name

    def get_display_name(self):
        return self.display_name

    def get_components(self):
        return self.components

    # setters
    def set_components(self, components: List[Component]):
        self.components = components
    
    def append_component_to_components(self, component: Component):
        self.components.append(component)
