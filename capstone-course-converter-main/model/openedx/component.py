import uuid
from enum import Enum
from abc import ABC, abstractmethod

class ComponentType(Enum):
    video = 1
    openassessment = 2
    problem = 3
    html = 4

class Component(ABC):
    def __init__(
        self,
        component_type: ComponentType = ComponentType.html,
        display_name: str = "Component"
        ):
        
        self.url_name = uuid.uuid4().hex
        self.component_type = component_type
        self.display_name = display_name

    # getters
    def get_url_name(self):
        return self.url_name

    def get_type(self):
        return self.component_type

    def get_display_name(self):
        return self.display_name

    # setters
    def set_url_name(self, url_name: str):
        self.url_name = url_name

    def set_component_type(self, component_type: ComponentType):
        self.component_type = component_type

    def set_display_name(self, name: str):
        self.display_name = name

class VideoComponent(Component):
    def __init__(self, *args, **kwargs):
        super(VideoComponent, self).__init__(*args, **kwargs)
        self.youtube_id = ""
        self.transcripts = "{}"
        self.edx_video_id = ""
        self.src = ""

    # getters
    def get_youtube_id(self):
        return self.youtube_id

    def get_transcripts(self):
        return self.transcripts

    def get_edx_video_id(self):
        return self.edx_video_id

    def get_src(self):
        return self.src

    # setters
    def set_youtube_id(self, youtube_id: str):
        self.youtube_id = youtube_id

    def set_edx_video_id(self, edx_video_id: str):
        self.edx_video_id = edx_video_id

    def set_src(self, src: str):
        self.src = src

class OpenAssessmentComponent(Component):
    def __init__(self, *args, **kwargs):
        super(OpenAssessmentComponent, self).__init__(*args, **kwargs)
        self.html_content = "<div>default assignment html description</div>"
        self.submission_start = "2023-01-01T00:00:00+00:00"
        self.submission_due = "2024-01-01T00:00:00+00:00"

    # getters
    def get_html_content(self):
        return self.html_content
    
    def get_start(self):
        return self.start

    def get_due(self):
        return self.due

    def get_submission_start(self):
        return self.submission_start

    def get_submission_due(self):
        return self.submission_due
    
    # setters
    def set_html_content(self, html_content: str):
        self.html_content = html_content

    def set_start(self, start: str):
        self.start = start

    def set_due(self, due: str):
        self.due = due

    def set_submission_start(self, submission_start: str):
        self.submission_start = submission_start

    def set_submission_due(self, submission_due: str):
        self.submission_due = submission_due

class ProblemComponent(Component):
    def __init__(self, *args, **kwargs):
        super(ProblemComponent, self).__init__(*args, **kwargs)

class HTMLComponent(Component):
    def __init__(self, *args, **kwargs):
        super(HTMLComponent, self).__init__(*args, **kwargs)
        self.html_content = "<div>default html content</div>"

    # getters
    def get_html_content(self):
        return self.html_content
    
    # setters
    def set_html_content(self, html_content: str):
        self.html_content = html_content