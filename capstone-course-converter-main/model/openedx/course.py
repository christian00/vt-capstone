import os
import xml.etree.ElementTree as ET
from model.openedx.chapter import Chapter

class OpenEdxDir:
    def __init__(self, dir_name: str):
        self.parent_dir = dir_name
        self.sub_dirs = [
            "about",
            "assets",
            "chapter",
            "course",
            "html",
            "info",
            "policies",
            "problem",
            "sequential",
            "static",
            "vertical",
            "video",
        ]

    def has_course_xml(self) -> bool:
        return os.path.exists(self.parent_dir + "/course.xml")

    def count_sub_directory(self) -> int:
        all_entries = os.listdir(self.parent_dir)

        # use a for loop to count the number of subdirectories in the parent directory
        num_subdirs = 0
        for entry in all_entries:
            entry_path = os.path.join(self.parent_dir, entry)
            if os.path.isdir(entry_path):
                num_subdirs += 1

        return num_subdirs

class OpenEdxCourse:
    def __init__(self, course_name: str, url_name: str, org: str) -> None:
        self.course_name = course_name
        self.url_name = url_name
        self.org = org
        self.course_number = course_name.lower().replace(" ", "-") # can not contain space
        
        # dictionary: [number#] to Chapter Object
        self.edxChapters = {} 