import os
import uuid
from typing import List
import xml.etree.ElementTree as ET

from model.openedx.vertical import Vertical

class Sequential:
    def __init__(self, display_name: str = "Subsection"):
        self.url_name = uuid.uuid4().hex
        self.display_name = display_name
        self.start_datetime = "2023-03-01T00:00:00+00:00"
        
        # create exactly on vertical for a sequential
        self.verticals: List[Vertical] = []

    # getters
    def get_url_name(self):
        return self.url_name

    def get_display_name(self):
        return self.display_name

    def get_start_datetime(self):
        return self.start_datetime

    def get_verticals(self):
        return self.verticals
    
    # setters
    def set_display_name(self, name: str):
        self.display_name = name

    def set_start_datetime(self, datetime: str):
        self.start_datetime = datetime

    def append_vertical_to_verticals(self, vertical: Vertical):
        self.verticals.append(vertical)
