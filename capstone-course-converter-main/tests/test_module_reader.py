import unittest
import xml.etree.ElementTree as ET

from model.canvas.assignment import Assignment
from model.canvas.item import Item
from model.canvas.module import Module

NS_DICT = {"cccv1p0": "http://canvas.instructure.com/xsd/cccv1p0"}


class ModuleReader:
    def __init__(self):
        self.module_root = None
        self.module = None

    def read(self, filename: str):
        module_tree = ET.parse(filename)
        self.module_root = module_tree.getroot()

    def parse_module(self):
        first_module = self.module_root[0]
        self.module = Module(
            identifier=first_module.attrib.get("identifier"),
            title=first_module.find("cccv1p0:title", namespaces=NS_DICT).text,
        )

    def parse_items(self):
        first_module = self.module_root[0]
        items = []
        for unparsed_item in first_module[5]:
            curr_item = Item(
                identifier=unparsed_item.get("identifier"),
                title=unparsed_item.find("cccv1p0:title", namespaces=NS_DICT).text,
            )
            content_type = unparsed_item.find("cccv1p0:content_type", namespaces=NS_DICT)
            identifierref = unparsed_item.find("cccv1p0:identifierref", namespaces=NS_DICT)
            if content_type is not None:
                curr_item.set_content_type(content_type.text)
            if identifierref is not None:
                curr_item.set_identifierref(identifierref.text)
            items.append(curr_item)

        self.module.set_items(items)

    def parse_assignment(self, identifier: str) -> Assignment:
        assignment_tree = ET.parse(
            "canvas-exported-files/" + identifier + "/assignment_settings.xml"
        )
        assignment_root = assignment_tree.getroot()

        return Assignment(
            identifier=identifier,
            title=assignment_root.find("cccv1p0:title", namespaces=NS_DICT).text,
            due_at=assignment_root.find("cccv1p0:due_at", namespaces=NS_DICT).text,
        )

class TestModuleReader(unittest.TestCase):
    def setUp(self):
        self.reader = ModuleReader()

    def test_module_has_values_after_reading_xml_file(self):
        self.reader.read(filename="module_meta_test_file.xml")
        self.reader.parse_module()

        assert self.reader.module.identifier == "g9803c956240eeb781a87544c08340a5a"
        assert self.reader.module.title == "Week 1 — Phase I"

    def test_module_has_items_after_parsing_items(self):
        self.reader.read(filename="module_meta_test_file.xml")
        self.reader.parse_module()
        self.reader.parse_items()

        assert len(self.reader.module.items) > 0

    def test_get_assignment(self):
        dummy_identifierref = "g8cf9bed52123031c516d103e6ded33a6"

        assignment = self.reader.parse_assignment(dummy_identifierref)

        assert "Lecture: Introduction" in assignment.title
        assert assignment.due_at is not None
        assert assignment.identifier == dummy_identifierref

if __name__ == "__main__":
    unittest.main()
