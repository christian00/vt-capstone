import shutil
import unittest
import xml.etree.ElementTree as ET
from unittest.mock import MagicMock, Mock

from app.course_converter import CourseConverter
from app.openedx_course_creator import (
    OpenEdxCourseCreator,
    OpenEdxCourseCreatorInterface,
)
from model.canvas.item import ContentType, Item
from model.canvas.module import Module
from model.openedx.chapter import Chapter
from model.openedx.sequential import Sequential


class MockCreator(OpenEdxCourseCreatorInterface):
    def append_sequential(self, seq_item: Sequential, chapter: Chapter):
        pass


class TestCourseConverter(unittest.TestCase):
    def setUp(self):
        self.converter = CourseConverter(OpenEdxCourseCreator())

    def test_convert_module_to_chapter(self):
        module = Module(title="Week2 ABC")

        self.converter.convert_module_to_chapter(module)

        chapter = self.converter.course_creator.course.chapters[0]
        tree = ET.parse(
            self.converter.course_creator.openedx_dir.parent_dir
            + "/chapter/"
            + chapter.url_name
            + ".xml"
        )
        chapter_root = tree.getroot()
        assert module.title == chapter_root.get("display_name")

    def test_item_title_should_convert_to_vertical_display_name(self):
        item = Item(title="Team Assignment 1")

        self.converter.convert_module_item_to_vertical(item)

        tree = ET.parse(
            self.converter.course_creator.openedx_dir.parent_dir
            + "/vertical/"
            + item.identifier
            + ".xml"
        )
        vertical_root = tree.getroot()
        assert item.title == vertical_root.get("display_name")

    def test_item_is_assignment_should_convert_to_vertical_has_openassessment_tag(
        self,
    ):
        item = Item(content_type=ContentType.Assignment, title="Team Assignment 1")

        self.converter.convert_module_item_to_vertical(item)

        tree = ET.parse(
            self.converter.course_creator.openedx_dir.parent_dir
            + "/vertical/"
            + item.identifier
            + ".xml"
        )
        vertical_root = tree.getroot()
        assert vertical_root.findall("openassessment")

    def test_item_is_not_assignment_should_not_have_openassessment_tag(
        self,
    ):
        item = Item(content_type=ContentType.WikiPage, title="Team Assignment 1")

        self.converter.convert_module_item_to_vertical(item)

        tree = ET.parse(
            self.converter.course_creator.openedx_dir.parent_dir
            + "/vertical/"
            + item.identifier
            + ".xml"
        )
        vertical_root = tree.getroot()
        assert not vertical_root.findall("openassessment")

    def test_item_should_convert_to_sequential(self):
        creator = Mock()
        converter = CourseConverter(creator)
        module = Module(title="Chapter", items=[Item(title="subsection1")])
        creator.append_sequential = MagicMock(name="append_sequential")

        converter.convert_module_to_chapter(module)

        creator.append_sequential.assert_called_once()

    def tearDown(self) -> None:
        shutil.rmtree("deault-exported-course") # change name


if __name__ == "__main__":
    unittest.main()
