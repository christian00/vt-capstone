import os
import shutil
import unittest
import xml.etree.ElementTree as ET

from model.openedx.chapter import Chapter
from model.openedx.sequential import Sequential
from model.openedx.vertical import Vertical
from app.openedx_course_creator import OpenEdxCourseCreator


class TestOpenEdxExporter(unittest.TestCase):
    def setUp(self):
        self.course_creator = OpenEdxCourseCreator()

    def test_opedirectory_has_11_subdirectories(self):
        directory = self.course_creator.create_directory()

        assert directory.count_sub_directory() == 11  # meaningless (improve later)

    def test_create_course(self):
        self.course_creator.create_directory()

        self.course_creator.create_course(course_name="CS50")

        assert self.course_creator.has_course_xml() is True
        assert os.path.exists(
            self.course_creator.openedx_dir.parent_dir
            + "/course/"
            + self.course_creator.course.url_name
            + ".xml"
        )

    def test_chapter_xml_file_should_exist_after_creating_chapter(self):
        self.course_creator.create_directory()
        self.course_creator.create_course(course_name="CS50")

        self.course_creator.append_chapter(Chapter(), self.course_creator.course)

        assert os.path.exists(
            self.course_creator.openedx_dir.parent_dir
            + "/chapter/"
            + self.course_creator.course.chapters[0].url_name
            + ".xml"
        )

    def test_chapter_display_name_is_same_as_given_after_creating_chapter(self):
        self.course_creator.create_directory()
        self.course_creator.create_course(course_name="CS50")

        self.course_creator.append_chapter(Chapter(), self.course_creator.course)

        tree = ET.parse(
            self.course_creator.openedx_dir.parent_dir
            + "/chapter/"
            + self.course_creator.course.chapters[0].url_name
            + ".xml"
        )
        chapter_root = tree.getroot()
        assert (
            chapter_root.get("display_name")
            == self.course_creator.course.chapters[0].display_name
        )

    def test_append_two_sequentials_in_chapter(self):
        self.prepare_course_and_chapters()
        items = [Sequential(), Sequential()]
        self.course_creator.append_sequential(
            items[0], self.course_creator.course.chapters[0]
        )
        self.course_creator.append_sequential(
            items[1], self.course_creator.course.chapters[0]
        )

        tree = ET.parse(
            self.course_creator.openedx_dir.parent_dir
            + "/chapter/"
            + self.course_creator.course.chapters[0].url_name
            + ".xml"
        )
        chapter_root = tree.getroot()
        assert len(chapter_root) == 2
        for idx, child in enumerate(chapter_root):
            assert child.attrib["url_name"] == items[idx].url_name

    def test_sequential_id_xml_exist_after_append_sequential_in_chapter(self):
        self.prepare_course_and_chapters()
        seq_item = Sequential()

        self.course_creator.append_sequential(
            seq_item, self.course_creator.course.chapters[0]
        )

        assert os.path.exists(
            self.course_creator.openedx_dir.parent_dir
            + "/sequential/"
            + seq_item.url_name
            + ".xml"
        )

    def test_vertical_id_xml_exist_after_append_vertical_in_sequential(self):
        self.prepare_course_and_chapters()
        seq_item = Sequential()
        self.course_creator.append_sequential(
            seq_item, self.course_creator.course.chapters[0]
        )
        vertical = Vertical()

        self.course_creator.append_vertical(vertical, seq_item)

        assert os.path.exists(
            self.course_creator.openedx_dir.parent_dir
            + "/vertical/"
            + vertical.url_name
            + ".xml"
        )

    def test_vertical_tag_exist_after_append_vertical_in_sequential(self):
        self.prepare_course_and_chapters()
        seq_item = Sequential()
        self.course_creator.append_sequential(
            seq_item, self.course_creator.course.chapters[0]
        )
        vertical = Vertical()

        self.course_creator.append_vertical(vertical, seq_item)

        tree = ET.parse(
            self.course_creator.openedx_dir.parent_dir
            + "/sequential/"
            + seq_item.url_name
            + ".xml"
        )
        seq_root = tree.getroot()
        assert seq_root[0].attrib["url_name"] == vertical.url_name

    def prepare_course_and_chapters(self):
        self.course_creator.create_directory()
        self.course_creator.create_course(course_name="CS50")
        self.course_creator.append_chapter(Chapter(), self.course_creator.course)

    def tearDown(self) -> None:
        shutil.rmtree("deault-exported-course") # change static name


if __name__ == "__main__":
    unittest.main()
