import os
import tarfile

from canvasParser.canvas_course_parser import CanvasCourseParser
from courseConverter.course_converter import CourseConverter
from openedxCreator.openedx_course_creator import OpenEdxCourseCreator

STATIC_CANVAS_EXPORT_ROOT_DIR_NAME = "canvas-exported-files"
UNZIPPED_CANVAS_EXPORT_ROOT_DIR_NAME = "unzipped-canvas-exported-files"
EDX_IMPORT_ROOT_DIR_NAME = "edx-import-folder"

if __name__ == "__main__":
    # global variable
    course_parser = None
    course_converter = None
    course_creator = None

    """
    Parser
    (1) parse from Canvas exported file
    (2) create CanvasCourse object using parsed data
    """
    runParser = True
    unZipImcss = True
    if(runParser):
        if(unZipImcss):
            course_parser = CanvasCourseParser(UNZIPPED_CANVAS_EXPORT_ROOT_DIR_NAME)
            course_parser.get_unzipped_directory()
        else:
            course_parser = CanvasCourseParser(STATIC_CANVAS_EXPORT_ROOT_DIR_NAME)
            course_parser.set_static_directory()

        course_parser.create_course()

        course_parser.parse_course_settings_folder(printLog=False)
        course_parser.parse_imsmanifest_xml(printLog=False)

        course_parser.parse_all_identifier_folder(printLog=False)
        # course_parser.parse_web_resources_folder(printLog=False) 
        
        course_parser.process_items_in_modules(printLog=False)
        
        course_parser.show_parsed_modules_items()

    """
    Converter
    (1) new a OpenEdxCourse object
    (2) convert CanvasCourse object into OpenedXCourse object
    """
    runConverter = True
    if(runConverter):
        course_creator = OpenEdxCourseCreator(EDX_IMPORT_ROOT_DIR_NAME)
        course_creator.create_course(
            course_name=course_parser.course.course_name,
            url_name="2023Spring",
            org="VT"
        )

        # create converter
        course_converter = CourseConverter(course_parser.course,course_creator.course)
        
        # convert
        course_converter.convert_modules_to_chapters()
    
    """
    Creator
    (1) create tar.gz import file from OpenEdxCourse object
    """
    runConverter = True
    runTar = True
    if(not runConverter):
        if(runTar):
            # only tar
            cwd = os.getcwd()
            targetFolder = (cwd+'/'+ course_creator.COURSE_ROOT_DIR_NAME)
            with tarfile.open(course_creator.COURSE_ROOT_DIR_NAME+'.tar.gz', "w:gz") as tar:
                tar.add(targetFolder, arcname=os.path.basename(targetFolder))
    else:
        # create root dorectory for export file
        course_creator.create_directory()

        # create /course.xml
        course_creator.create_top_course_xml()

        # create /course/[coursename].xml
        course_creator.create_course_xml()
        
        # create /chapter/[id].xml and append chapter element to /course/{coursename}.xml
        # recursively create sequentials, verticals and components
        course_creator.add_chapters(course_creator.course)
        
        # append wiki element to /course/[coursename].xml after chapters is appended
        course_creator.append_wiki(course_creator.course)
        
        # /about/overview.html
        course_creator.create_about()

        # /assets/assets.xml
        course_creator.create_assets()
        
        # /info/updates.xml
        course_creator.create_info()

        if(runTar):
            course_creator.make_tarfile()
