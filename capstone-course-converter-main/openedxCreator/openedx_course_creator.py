import os
import tarfile
import xml.etree.ElementTree as ET
from abc import ABC, abstractmethod

from model.openedx.chapter import Chapter
from model.openedx.course import OpenEdxCourse, OpenEdxDir
from model.openedx.sequential import Sequential
from model.openedx.vertical import Vertical
from model.openedx.component import ComponentType, Component, VideoComponent, OpenAssessmentComponent, ProblemComponent, HTMLComponent

from openedxCreator.subCreator.about_creator import create_overview_html_under
from openedxCreator.subCreator.asset_creator import create_assets_xml_under
from openedxCreator.subCreator.info_creator import create_updates_html_under
from openedxCreator.subCreator.video_creator import create_yt_video_xml_under, create_static_video_xml_under
from openedxCreator.subCreator.openassessment_creator import append_openassessment_to

class OpenEdxCourseCreatorInterface(ABC):
    @abstractmethod
    def create_directory(self) -> OpenEdxDir:
        pass

    @abstractmethod
    def create_course(
        self, course_name: str, url_name: str = "Spring2023", org: str = "VT"
    ) -> None:
        pass

    @abstractmethod
    def create_top_course_xml(self):
        pass

    @abstractmethod
    def append_sequential(self, seq_item: Sequential, chapter: Chapter):
        pass

class OpenEdxCourseCreator:
    COURSE_ROOT_DIR_NAME = "edx-import-folder"
    COURSE_XML_FILENAME = "course.xml"

    def __init__(self, COURSE_ROOT_DIR_NAME: str = "edx-import-folder"):
        self.course: OpenEdxCourse = None
        self.openedx_dir: OpenEdxDir = None
        self.COURSE_ROOT_DIR_NAME = COURSE_ROOT_DIR_NAME

        if(COURSE_ROOT_DIR_NAME.strip()):
            # if dir_name is specified in constructor
            self.COURSE_ROOT_DIR_NAME = COURSE_ROOT_DIR_NAME

    def create_directory(self) -> OpenEdxDir:
        # create the folder 'course' if it doesn't exist already
        if not os.path.exists(self.COURSE_ROOT_DIR_NAME):
            os.mkdir(self.COURSE_ROOT_DIR_NAME)

        openedx_dir = OpenEdxDir(dir_name=self.COURSE_ROOT_DIR_NAME)

        # create the sub directories
        for sub_dir in openedx_dir.sub_dirs:
            os.mkdir(self.COURSE_ROOT_DIR_NAME + '/' + sub_dir)
        self.openedx_dir = openedx_dir
        return openedx_dir

    def create_course(
        self, course_name: str, url_name: str = "2023Spring", org: str = "VT"
    ) -> None:
        self.course = OpenEdxCourse(course_name, url_name, org)

    """
    # create /course.xml
    """
    # /course.xml
    def create_top_course_xml(self):
        course_ele = ET.Element("course")
        course_ele.set("url_name", self.course.url_name)
        course_ele.set("org", self.course.org)
        course_ele.set("course", self.course.course_number)
        tree = ET.ElementTree(course_ele)
        tree.write(os.path.join(self.COURSE_ROOT_DIR_NAME, self.COURSE_XML_FILENAME))

    """
    # create /course/[course_name].xml
    """
    # /course/[course_name].xml
    def create_course_xml(self):
        # below are default fixed value
        course_root = ET.Element("course")
        course_root.set("cert_html_view_enabled", "true")
        course_root.set("display_name", self.course.course_name)
        course_root.set("language", "en")
        course_root.set("start", "2030-01-01T00:00:00+00:00")

        # will append/specify chapter & wiki later
        course_tree = ET.ElementTree(course_root)
        ET.indent(course_tree, space="\t", level=0)
        course_tree.write(self.COURSE_ROOT_DIR_NAME + "/course/" + self.course.url_name + ".xml")
    
    # update wiki to /course/[course_name].xml
    def append_wiki(self, course: OpenEdxCourse):
        course_filename = course.url_name + ".xml"
        self.add_wiki_tag_to(self.COURSE_ROOT_DIR_NAME + "/course/" + course_filename)

    def add_wiki_tag_to(self, filename: str):
        tree = ET.parse(filename)
        course_root = tree.getroot()

        wiki_ele = ET.SubElement(course_root, "wiki")
        wiki_ele.set(
            "slug",
            f"{self.course.org}.{self.course.course_name}.{self.course.url_name}",
        )

        tree = ET.ElementTree(course_root)
        ET.indent(tree, space="\t", level=0)
        tree.write(filename)
   
    """
    # update /about
    """
    # /about/overview.html
    def create_about(self):
        create_overview_html_under(self.COURSE_ROOT_DIR_NAME + "/about/")

    """
    # update /assets
    """
    # /assets/assets.xml
    def create_assets(self):
        create_assets_xml_under(self.COURSE_ROOT_DIR_NAME + "/assets/")

    """
    # update /info
    """
    # /info/update.html
    def create_info(self):
        create_updates_html_under(self.COURSE_ROOT_DIR_NAME + "/info/")

    """
    # add chapters
    """
    def add_chapters(self, course: OpenEdxCourse):
        for index in course.edxChapters:
            chapter = course.edxChapters[index]
            self.append_chapter_to_course(chapter)
            self.create_chapter_xml(chapter)
            self.add_sequentials(chapter)

    # update /course/[course.url_name].xml
    def append_chapter_to_course(self, chapter: Chapter):
        filepath = self.COURSE_ROOT_DIR_NAME + "/course/" + self.course.url_name + ".xml"
        tree = ET.parse(filepath)
        root = tree.getroot()
        chapter_element = ET.SubElement(root, "chapter")
        chapter_element.set("url_name", chapter.get_url_name())
        
        tree = ET.ElementTree(root)
        ET.indent(tree, space="\t", level=0)
        tree.write(filepath)

    # create /chapter/[url_name].xml
    def create_chapter_xml(self, chapter: Chapter):
        filepath = self.COURSE_ROOT_DIR_NAME + "/chapter/" + chapter.get_url_name() + ".xml"
        root = ET.Element("chapter")
        root.set("display_name", chapter.get_display_name())

        tree = ET.ElementTree(root)
        ET.indent(tree, space="\t", level=0)
        tree.write(filepath)
    
    """
    # add sequentials
    """
    def add_sequentials(self, chapter: Chapter):
        for sequential in chapter.get_sequentials():
            self.append_sequential_to_chapter(sequential, chapter)
            self.create_sequential_xml(sequential)
            self.add_verticals(sequential)

    # update /chapter/[url_name].xml
    def append_sequential_to_chapter(self, sequential: Sequential, chapter: Chapter):
        filepath = self.COURSE_ROOT_DIR_NAME + "/chapter/" + chapter.get_url_name() + ".xml"
        tree = ET.parse(filepath)
        root = tree.getroot()
        sequential_element = ET.SubElement(root, "sequential")
        sequential_element.set("url_name", sequential.get_url_name())
        
        tree = ET.ElementTree(root)
        ET.indent(tree, space="\t", level=0)
        tree.write(filepath)

    # create /sequential/[url_name].xml
    def create_sequential_xml(self, sequential: Sequential):
        filepath = self.COURSE_ROOT_DIR_NAME + "/sequential/" + sequential.get_url_name() + ".xml"
        root = ET.Element("sequential")
        root.set("display_name", sequential.get_display_name())
        root.set("start", sequential.get_start_datetime())

        tree = ET.ElementTree(root)
        ET.indent(tree, space="\t", level=0)
        tree.write(filepath)

    """
    # add verticals
    """
    def add_verticals(self, sequential: Sequential):
        for vertical in sequential.get_verticals():
            self.append_vertical_to_sequential(vertical, sequential)
            self.create_vertical_xml(vertical)
            self.add_components(vertical)

    # update /sequential/[url_name].xml
    def append_vertical_to_sequential(self, vertical: Vertical, sequential: Sequential):
        filepath = self.COURSE_ROOT_DIR_NAME + "/sequential/" + sequential.get_url_name() + ".xml"
        tree = ET.parse(filepath)
        root = tree.getroot()
        vertical_element = ET.SubElement(root, "vertical")
        vertical_element.set("url_name", vertical.get_url_name())
        
        tree = ET.ElementTree(root)
        ET.indent(tree, space="\t", level=0)
        tree.write(filepath)

    # create /vertical/[url_name].xml
    def create_vertical_xml(self, vertical: Vertical):
        filepath = self.COURSE_ROOT_DIR_NAME + "/vertical/" + vertical.get_url_name() + ".xml"
        root = ET.Element("vertical")
        root.set("display_name", vertical.get_display_name())

        tree = ET.ElementTree(root)
        ET.indent(tree, space="\t", level=0)
        tree.write(filepath)

    """
    # add components
    """
    def add_components(self, vertical: Vertical):
        for component in vertical.get_components():
            self.append_component_to_vertical(component, vertical)
            self.create_component_xml(component)

    # update /vertical/[url_name].xml
    def append_component_to_vertical(self, component: Component, vertical: Vertical):
        if(component.component_type == ComponentType['video']):
            self.append_video_component(component, vertical)
        elif(component.component_type == ComponentType['openassessment']):
            self.append_openassessment_component(component, vertical)
        elif(component.component_type == ComponentType['problem']):
            self.append_problem_component(component, vertical)
        elif(component.component_type== ComponentType['html']):
            self.append_html_component(component, vertical)
        else:
            None

    def append_video_component(self, component: VideoComponent, vertical: Vertical):
        filepath = self.COURSE_ROOT_DIR_NAME + "/vertical/" + vertical.get_url_name() + ".xml"
        tree = ET.parse(filepath)
        root = tree.getroot()
        video_element = ET.SubElement(root, "video")
        video_element.set("url_name", component.get_url_name())
        
        tree = ET.ElementTree(root)
        ET.indent(tree, space="\t", level=0)
        tree.write(filepath)

    def append_openassessment_component(self, component: OpenAssessmentComponent, vertical: Vertical):
        filepath = self.COURSE_ROOT_DIR_NAME + "/vertical/" + vertical.get_url_name() + ".xml"
        append_openassessment_to(self, component=component, filepath=filepath)

    def append_problem_component(self, component: ProblemComponent, vertical: Vertical):
        filepath = self.COURSE_ROOT_DIR_NAME + "/vertical/" + vertical.get_url_name() + ".xml"
        tree = ET.parse(filepath)
        root = tree.getroot()
        problem_element = ET.SubElement(root, "problem")
        problem_element.set("url_name", component.get_url_name())
        
        tree = ET.ElementTree(root)
        ET.indent(tree, space="\t", level=0)
        tree.write(filepath)

    def append_html_component(self, component: HTMLComponent, vertical: Vertical):
        filepath = self.COURSE_ROOT_DIR_NAME + "/vertical/" + vertical.get_url_name() + ".xml"
        tree = ET.parse(filepath)
        root = tree.getroot()
        html_element = ET.SubElement(root, "html")
        html_element.set("url_name", component.get_url_name())
        
        tree = ET.ElementTree(root)
        ET.indent(tree, space="\t", level=0)
        tree.write(filepath)

    def create_component_xml(self, component: Component):
        if(component.component_type == ComponentType['video']):
            self.create_video_xml(component)
        elif(component.component_type == ComponentType['openassessment']):
            None #have updated vertical when doing append_openassessment_component()
        elif(component.component_type == ComponentType['problem']):
            self.create_problem_component(component)
        elif(component.component_type == ComponentType['html']):
            self.create_html_component(component)
        else:
            None

    # create /video/[url_name].html
    def create_video_xml(self, component: VideoComponent):
        filepath = self.COURSE_ROOT_DIR_NAME + "/video/" + component.get_url_name() + ".xml"
        isYT = True
        if(isYT):
            create_yt_video_xml_under(filepath, component)
        else:
            create_static_video_xml_under(filepath, component)
    
    # create /problem/[url_name].html
    def create_problem_component(self, component: ProblemComponent):
        filepath = self.COURSE_ROOT_DIR_NAME + "/problem/" + component.get_url_name() + ".xml"
        root = ET.Element("problem")
        root.set("display_name", component.get_display_name())
        # <choice response>, <optionresponse> ..., etc.
        tree = ET.ElementTree(root)
        ET.indent(tree, space="\t", level=0)
        tree.write(filepath)

    # create /html/[url_name].xml & [url_name].html
    def create_html_component(self, component: HTMLComponent):
        # [url_name].html
        htmlfilepath = self.COURSE_ROOT_DIR_NAME + "/html/" + component.get_url_name() + ".html"
        html_tree = ET.ElementTree(ET.fromstring(component.get_html_content()))
        ET.indent(html_tree, space="\t", level=0)
        html_tree.write(htmlfilepath)

        # /html/[url_name].xml
        xmlfilepath = self.COURSE_ROOT_DIR_NAME + "/html/" + component.get_url_name() + ".xml"
        root = ET.Element("html")
        root.set("filename", component.get_url_name())
        root.set("display_name", component.get_display_name())

        xml_tree = ET.ElementTree(root)
        ET.indent(xml_tree, space="\t", level=0)
        xml_tree.write(xmlfilepath)
    
    """
    Others
    """
    def has_course_xml(self) -> bool:
        return self.openedx_dir.has_course_xml()

    def get_course(self) -> OpenEdxCourse:
        return self.course

    def make_tarfile(self):
        cwd = os.getcwd()
        targetFolder = (cwd+'/'+self.COURSE_ROOT_DIR_NAME)
        with tarfile.open(self.COURSE_ROOT_DIR_NAME+'.tar.gz', "w:gz") as tar:
            tar.add(targetFolder, arcname=os.path.basename(targetFolder))