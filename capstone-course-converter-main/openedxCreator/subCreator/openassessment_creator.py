import xml.etree.ElementTree as ET

from model.openedx.component import OpenAssessmentComponent

def append_openassessment_to(self, component: OpenAssessmentComponent, filepath: str):
  tree = ET.parse(filepath)
  root = tree.getroot()
  openassessment_element = ET.SubElement(root, "openassessment")
  openassessment_element.set("url_name", component.get_url_name())
  update_openassessment(openassessment_element, component)
  
  tree = ET.ElementTree(root)
  ET.indent(tree, space="\t", level=0)
  tree.write(filepath)

def update_openassessment(openassessment, component: OpenAssessmentComponent):
  openassessment.set("submission_start", component.get_submission_start())
  openassessment.set("submission_due", component.get_submission_due())
  openassessment.set("text_response", "required")
  openassessment.set("text_response_editor", "text")
  openassessment.set("allow_multiple_files", "True")
  openassessment.set("allow_latex", "False")
  openassessment.set("prompts_type", "html")
  openassessment.set("teams_enabled", "False")
  openassessment.set("selected_teamset_id", "")
  openassessment.set("show_rubric_during_response", "False")

  title = ET.SubElement(openassessment, "title")
  title.text = "Open Response Assessment"

  assessments = ET.SubElement(openassessment, "assessments")
  assessment = ET.SubElement(assessments, "assessment")
  assessment.set("name", "staff-assessment")
  assessment.set("enable_flexible_grading", "False")
  assessment.set("required", "True")

  # prompts: assignment description
  prompts = ET.SubElement(openassessment, "prompts")
  prompt = ET.SubElement(prompts, "prompt")
  prompt_description = ET.SubElement(prompt, "description")
  prompt_description.text = component.get_html_content()

  # rubric: default criterions
  rubric = ET.SubElement(openassessment, "rubric")
  default_criterion = ET.SubElement(rubric, "criterion")
  default_criterion_name = ET.SubElement(default_criterion, "name")
  default_criterion_name.text = "Ideas"
  default_criterion_label = ET.SubElement(default_criterion, "label")
  default_criterion_label.text = "Ideas"
  default_criterion_prompt = ET.SubElement(default_criterion, "prompt")
  default_criterion_prompt.text = "Determine if there is a unifying theme or main idea."

  default_criterion_option1 = ET.SubElement(default_criterion, "option")
  default_criterion_option1.set("points", "0")
  default_criterion_option1_name = ET.SubElement(default_criterion_option1, "name")
  default_criterion_option1_name.text = "Poor"
  default_criterion_option1_label = ET.SubElement(default_criterion_option1, "label")
  default_criterion_option1_label.text = "Poor"
  default_criterion_option1_explanation = ET.SubElement(default_criterion_option1, "explanation")
  default_criterion_option1_explanation.text = "Difficult for the reader to discern the main idea.  Too brief or too repetitive to establish or maintain a focus."

  default_criterion_option2 = ET.SubElement(default_criterion, "option")
  default_criterion_option2.set("points", "3")
  default_criterion_option2_name = ET.SubElement(default_criterion_option2, "name")
  default_criterion_option2_name.text = "Fair"
  default_criterion_option2_label = ET.SubElement(default_criterion_option2, "label")
  default_criterion_option2_label.text = "Fair"
  default_criterion_option2_explanation = ET.SubElement(default_criterion_option2, "explanation")
  default_criterion_option2_explanation.text = "Presents a unifying theme or main idea, but may include minor tangents.  Stays somewhat focused on topic and task."

  default_criterion_option3 = ET.SubElement(default_criterion, "option")
  default_criterion_option3.set("points", "5")
  default_criterion_option3_name = ET.SubElement(default_criterion_option3, "name")
  default_criterion_option3_name.text = "Good"
  default_criterion_option3_label = ET.SubElement(default_criterion_option3, "label")
  default_criterion_option3_label.text = "Good"
  default_criterion_option3_explanation = ET.SubElement(default_criterion_option3, "explanation")
  default_criterion_option3_explanation.text = "Presents a unifying theme or main idea without going off on tangents.  Stays completely focused on topic and task."
  
  feedbackprompt = ET.SubElement(rubric, "feedbackprompt")
  feedbackprompt.text = "(Optional) What aspects of this response stood out to you? What did it do well? How could it be improved?"
  
  feedback_default_text = ET.SubElement(rubric, "feedback_default_text")
  feedback_default_text.text = "I think that this response..."
