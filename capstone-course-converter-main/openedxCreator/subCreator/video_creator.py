import xml.etree.ElementTree as ET

from model.openedx.component import VideoComponent

def create_yt_video_xml_under(filepath: str, component: VideoComponent):
  root = ET.Element("video")
  root.set("youtube", "1.00:"+component.get_youtube_id())
  root.set("url_name", component.get_url_name())
  root.set("transcripts", component.get_transcripts())
  root.set("edx_video_id", component.get_edx_video_id())
  root.set("youtube_id_1_0", component.get_youtube_id())

  video_asset = ET.SubElement(root, "video_asset")
  video_asset.set("client_video_id", "External Video")
  video_asset.set("duration", "0.0")
  video_asset.set("image", "")

  tree = ET.ElementTree(root)
  ET.indent(tree, space="\t", level=0)
  tree.write(filepath)

def create_static_video_xml_under(filepath: str, component: VideoComponent):
  root = ET.Element("video")
  root.set("url_name", component.get_url_name())
  root.set("display_name", component.get_display_name())
  root.set("html5_sources", "[&quot;"+ component.get_src() +"&quot;]")
  root.set("youtube_id_1_0", component.get_youtube_id())

  source = ET.SubElement(root, "source")
  source.set("src", component.get_src())

  tree = ET.ElementTree(root)
  ET.indent(tree, space="\t", level=0)
  tree.write(filepath)
