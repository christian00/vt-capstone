import xml.etree.ElementTree as ET

from model.openedx.course import OpenEdxCourse, OpenEdxDir

def create_assets_xml_under(directory: str):
  root = ET.Element("assets")
  root_tree = ET.ElementTree(root)
  ET.indent(root_tree, space="\t", level=0)
  root_tree.write(directory + "assets.xml")