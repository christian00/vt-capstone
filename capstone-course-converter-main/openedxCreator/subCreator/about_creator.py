import xml.etree.ElementTree as ET

from model.openedx.course import OpenEdxCourse, OpenEdxDir

def create_overview_html_under(directory: str):
    # fake root: div 
    root = ET.Element("div")

    # about
    sec_about = ET.Element("section")
    sec_about.set("class", "about")

    ET.SubElement(sec_about, "h2").text = "About This Course"
    ET.SubElement(sec_about, "p").text = "Include your long course description here. The long course description should contain 150-400 words."
    ET.SubElement(sec_about, "p").text= "This is paragraph 2 of the long course description. Add more paragraphs as needed. Make sure to enclose them in paragraph tags."
    
    # prerequisities
    sec_preq = ET.Element("section")
    sec_preq.set("class", "prerequisites")
    ET.SubElement(sec_preq, "h2").text = "Requirements"
    ET.SubElement(sec_preq, "p").text = "Add information about the skills and knowledge students need to take this course."
    
    # course-staff
    sec_staff = ET.Element("section")
    sec_staff.set("class", "course-staff")
    ET.SubElement(sec_staff, "h2").text = "Course Staff"
    # for instrcutor in instructors
    for i in range(3):
        article = ET.SubElement(sec_staff, "article")
        article.set("class", "teacher")
        
        image_div = ET.SubElement(article, "div")
        image_div.set("class", "teacher-image")
        
        image = ET.SubElement(image_div, "img")
        image.set("src", "/static/images/placeholder-faculty.png")
        image.set("align", "left")
        image.set("style", "margin:0 20 px 0")
        image.set("alt", "Course Staff Image #"+str(i))

        ET.SubElement(article, "h3").text = ("Staff #"+ str(i) +" Name")
        ET.SubElement(article, "p").text = "Staff Biography"

    # faq
    sec_faq = ET.Element("section")
    sec_faq.set("class", "faq")
    resps = ET.SubElement(sec_faq, "section")
    resps.set("class", "responses")
    ET.SubElement(resps, "h2").text = "Frequently Asked Questions"
    
    # for resp in responses
    for i in range(2):
        article = ET.SubElement(resps, "article")
        article.set("class", "response")

        ET.SubElement(article, "h3").text = "Question #"+str(i)
        ET.SubElement(article, "p").text = "Answer #"+str(i)

    # generating overview.html
    root.append(sec_about)
    root.append(sec_preq)
    root.append(sec_staff)
    root.append(sec_faq)

    root_tree = ET.ElementTree(root)
    ET.indent(root_tree, space="\t", level=0)
    root_tree.write(directory + "overview.html")


