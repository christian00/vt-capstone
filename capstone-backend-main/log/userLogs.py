import re
import json
import csv

# TODO: connect to openedx log location
file = open("data/tracking.log", "r")

# logs for logged in users only
data = []
for line in file:
    record = json.loads(re.search('({.+)', line)[0])
    if record["username"] != '':
        data.append(record)

# write records to csv
new_csv = open('log.csv', 'w', newline='\n')
w = csv.writer(new_csv)
w.writerow(['Name', 'Context', 'Username', 'Session', 'IP', 'Agent',
            'Host', 'Referer', 'Accept Language', 'Event', 'Time',
            'Event', 'Type', 'Event Source', 'Page'])
for record in data:
    w.writerow(record.values())
new_csv.close()
