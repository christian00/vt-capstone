import re
import json
import csv


# define fields to retrieve
class Session:
    session_name = ''
    username = ''
    course_id = ''
    session_start = ''
    session_end = ''

    def __iter__(self):
        return iter([self.session_name, self.username, self.course_id,
                     self.session_start, self.session_end])


# TODO: connect to openedx log location
file = open("data/lms-tracking.log", "r")

# logs for logged in users inside a course only
data = []
for line in file:
    record = json.loads(re.search('({.+)', line)[0])
    if record["username"] != '' and record["context"]["course_id"] != '':
        data.append(record)

# one record per session per course
unique_sessions = []
for record in data:
    if any(x.session_name == record["session"]
           and x.course_id == record["context"]["course_id"]
           for x in unique_sessions) is False:
        s = Session()
        s.session_name = record["session"]
        s.username = record["username"]
        s.course_id = record["context"]["course_id"]
        s.session_start = record["time"]
        s.session_end = record["time"]
        unique_sessions.append(s)
    else:
        # update session end time w/ latest record
        s = next(x for x in unique_sessions
                 if x.session_name == record["session"]
                 and x.course_id == record["context"]["course_id"])
        s.session_end = record["time"]

# write records to csv
session_csv = open('course_session_log.csv', 'w', newline='\n')
w = csv.writer(session_csv)
w.writerow(['Session Name', 'Username', 'Course ID', 'Session Start',
            'Session End'])
for session in unique_sessions:
    w.writerow(list(session))
session_csv.close()
