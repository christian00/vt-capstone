from pymongo import database
import pymongo
import config

class MongoDBConnector:
  def __init__(self):
    self.client = pymongo.MongoClient(config.mongo_url)
    self.db = self.client[config.mongo_db]

  def getClient(self):
    return self.client

  def getDB(self):
    return self.db

  def getCollection(self, collection_name):
    return self.db[collection_name]
  
  def showCollections(self):
    print(self.db.list_collection_names())

mongo_connector = MongoDBConnector()