import pymysql
import config

class MySQLConnector:
    def __init__(self):
        self.connection = pymysql.connect(
            host=config.mysql_host,
            port=int(config.mysql_port),
            user=config.mysql_user,
            password=config.mysql_pwd,
            db=config.mysql_db,
            cursorclass=pymysql.cursors.DictCursor,
        )

    def get_connection(self):
        return self.connection

mysql_connector = MySQLConnector()