from dotenv import dotenv_values
env = dotenv_values(".env")

frontend_local = env.get('FRONTEND_LOCAL')
mysql_host = env.get('MYSQL_HOST')
mysql_port = env.get('MYSQL_PORT')
mysql_user = env.get('MYSQL_USER')
mysql_pwd = env.get('MYSQL_PASSWORD')
mysql_db = env.get('MYSQL_DB')

mongo_url = env.get('MONGO_URL')
mongo_host = env.get('MONGO_HOST')
mongo_port = env.get('MONGO_PORT')
mongo_user = env.get('MONGO_USER')
mongo_pwd = env.get('MONGO_PASSWORD')
mongo_db = env.get('MONGO_DB')