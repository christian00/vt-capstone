from flask import Flask, Blueprint, jsonify, request
from api.service.quizService import QuizService

from fake.quiz import fake_quiz_data, fake_duration_data
from fake.user import fake_user_name
from fake.random import random_duration, random_number_between
from fake.date import millisecond_to_date

quiz_api = Blueprint('quiz_api', __name__)
quiz_service = QuizService()

# http://127.0.0.1:5000/v1/api/quizzes?course_id=1
@quiz_api.route('/', methods=['GET'])
def get_course_quizzes():
    course_id = request.args.get('course_id')
    matching_quizzes = []

    useRealData = True
    if(useRealData):
        matching_quizzes = quiz_service.find_quizzes_by_course(course_id)
    else:
        for quiz in fake_quiz_data:
            if quiz['course_id'] == course_id:
                matching_quizzes.append(quiz)
    
    response = jsonify(matching_quizzes)
    return response

# http://127.0.0.1:5000/v1/api/quizzes/duration?quiz_id=1
@quiz_api.route('/duration', methods=['GET'])
def get_quiz_duration():
    quiz_id = request.args.get('quiz_id')
    matching_duration = []
    
    useRealData = True
    if(useRealData):
        user_scores = quiz_service.find_quiz_data(quiz_id)
        if(user_scores):
            for ele in user_scores:
                if(ele['possible_all'] > 0):
                    grade = ele['earned_all']/ ele['possible_all'] * 100
                    if (grade < 60):
                        result = 2
                    elif(grade < 80):
                        result = 1
                    else:
                        result = 0
                    matching_duration.append({'id': ele['user_id'],
                                            'user': ele['username'],
                                            'duration': random_duration(120),
                                            'result': result,
                                            'score': grade,
                                            'quiz_id': quiz_id})
    else:
        # generate random duration
        for quiz in fake_quiz_data:
            if quiz['id'] == int(quiz_id):
                for i in range(0, len(fake_user_name)):  
                    matching_duration.append({'id': i, 'user': fake_user_name[i], 'duration': random_duration(quiz['limit']), 'result': random_number_between(0, 2), 'quiz_id': quiz_id})
    
    response = jsonify(matching_duration)
    return response