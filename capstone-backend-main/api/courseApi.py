import requests
from flask import Flask, Blueprint, jsonify, request
from api.service.courseService import CourseService
from api.service.userService import UserService
from api.service.ipService import get_course_ip_data_by_course

from fake.course import fake_courses_data
from fake.user import fake_user_name
from fake.ip import fake_ip_logs 
from fake.ml import fake_ml_data
from fake.random import random_number_between

course_api = Blueprint('course_api', __name__)
course_service = CourseService()
user_service = UserService()

# http://127.0.0.1:5000/v1/api/courses?instructor_email=hychien2000@gmail.com
@course_api.route('/', methods=['GET'])
def get_courses():
    instructor_name = request.args.get('instructor_name')
    instructor_email = request.args.get('instructor_email')
    matching_result = { 'email': instructor_email, 'username': instructor_name, 'courses': None}
    courses_data = []

    useRealData = True
    if(useRealData):
        if instructor_name:
            result = course_service.get_courses_by_name(instructor_name)
            if result:
                courses_data.extend(result)
            else:
                return jsonify({'message': 'user not exist'}), 400
            
            user_result = user_service.get_user_by_username(instructor_name)
            if user_result:
                matching_result['email'] = user_result['email']

        if instructor_email:
            result = course_service.get_courses_by_email(instructor_email)
            if result:
                courses_data.extend(result)
            else:
                return jsonify({'message': 'user not exist'}), 400

            user_result = user_service.get_user_by_email(instructor_email)
            if user_result:
                matching_result['username'] = user_result['username']
    else:
        for course in fake_courses_data:
            if course['instructor'] == instructor_name or course['email'] == instructor_email:
                courses_data.append({'mysql_id': course['mysql_id'], 'mongo_id': course['mongo_id'], 'name': course['name'], 'start': course['start'], 'end': course['end'], 'value': course['name']})

    matching_result['courses'] = courses_data
    response = jsonify(matching_result)
    return response

# http://127.0.0.1:5000/v1/api/courses/ips?course_id=1
@course_api.route('/ips', methods=['GET'])
def get_course_ips():
    course_id = request.args.get('course_id')
    matching_logs = []

    useRealData = True
    if(useRealData):
        ip_logs = get_course_ip_data_by_course(course_id.replace(" ", "+"))
        for log in ip_logs:
            payload = {
                'key': '5560C97C2DFAECFFE2CCFEF776090EFD',
                'ip': log['ip'],
                'format': 'json'
            }
            api_result = requests.get('https://api.ip2location.io/', params=payload)
            matching_logs.append({'user': log['user'], 'ip': log['ip'], 'timestamp': log['timestamp'], 'geoResult': api_result.json()})
    else:
        for log in fake_ip_logs:
            payload = {
                'key': '5560C97C2DFAECFFE2CCFEF776090EFD',
                'ip': log['ip'],
                'format': 'json'
            }
            api_result = requests.get('https://api.ip2location.io/', params=payload)
            matching_logs.append({'user': log['user'], 'ip': log['ip'], 'timestamp': log['timestamp'], 'geoResult': api_result.json()})

    response = jsonify(matching_logs)
    return response

# http://127.0.0.1:5000/v1/api/courses/ml?course_id=1
@course_api.route('/ml', methods=['GET'])
def get_course_ml_results():
    course_id = request.args.get('course_id')
    matchings = []

    useRealData = True
    if(useRealData):
        for i in range(0, len(fake_user_name)):  
            matchings.append({'user': fake_user_name[i],'result': random_number_between(0, 2),'course_id': course_id})
    else:
        for ml in fake_ml_data:
            if ml['course_id'] == course_id:
                matchings.append(ml)
    
    response = jsonify(matchings)
    return response