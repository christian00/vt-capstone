from flask import Flask, Blueprint, jsonify, request
from api.service.courseService import CourseService
from api.service.activityService import getStudentSessions

from fake.course import fake_courses_data
from fake.random import random_date, random_activity_count
from fake.date import millisecond_to_hour

activity_api = Blueprint('activity_api', __name__)
course_service = CourseService()

# http://127.0.0.1:5000/v1/api/activities?course_id=1
@activity_api.route('/', methods=['GET'])
def get_course_activites():
    course_id = request.args.get('course_id')
    matching_activities = []

    useRealData = True
    if(useRealData):
        for i in range(0, 60):   
            course = course_service.get_course_by_mongo_id(course_id)
            msecond = random_date(course['start'], course['end'], time_format="%Y-%m-%d %H:%M:%S")
            matching_activities.append([msecond, random_activity_count()])

    else:
        for course in fake_courses_data:
            if course['mongo_id'] == course_id:
                for i in range(0, 60):   
                    msecond = random_date(course['start'], course['end'])
                    matching_activities.append([msecond, random_activity_count()])
    
    matching_activities = sorted(matching_activities, key=lambda x: x[0])
    response = jsonify(matching_activities)
    return response

# http://127.0.0.1:5000/v1/api/activities/sessions?course_id=1
@activity_api.route('/sessions', methods=['GET'])
def get_course_sessions():
    course_id = request.args.get('course_id')
    matching_sessions = []

    matching_sessions = getStudentSessions(course_id.replace(" ", "+"))
    
    matching_sessions = sorted(matching_sessions, key=lambda x: x['Duration'])
    response = jsonify(matching_sessions)
    return response