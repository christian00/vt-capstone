import random
from flask import Flask, Blueprint, jsonify, request
from api.service.assignmentService import AssignmentService

from fake.assignment import fake_assignment_data, fake_submission_data
from fake.random import random_date, random_number_between
from fake.date import millisecond_to_date
from fake.user import fake_user_name

assignment_api = Blueprint("assignment_api", __name__)
assignment_service = AssignmentService()


# http://127.0.0.1:5000/v1/api/assignments?course_id=1
@assignment_api.route("/", methods=["GET"])
def get_course_assignments():
    course_id = request.args.get("course_id")
    matching_assignments = []

    useRealData = True
    if useRealData:
        matching_assignments = assignment_service.find_assignments_by_course(course_id)
    else:
        for assignment in fake_assignment_data:
            if assignment["course_id"] == course_id:
                matching_assignments.append(assignment)

    response = jsonify(matching_assignments)
    return response


# http://127.0.0.1:5000/v1/api/assignments/submissions?assignment_id=1
@assignment_api.route("/submissions", methods=["GET"])
def get_assignment_submissions():
    course_id = request.args.get("course_id")
    assignment_id = request.args.get("assignment_id")
    matching_submissions = []

    useRealData = True
    if useRealData:
        assignment = assignment_service.find_assignment_by_id(course_id, assignment_id)
        msecond = random_date(
            assignment["start"],
            assignment["end"],
            time_format="%Y-%m-%dT%H:%M:%S+00:00",
        )
        user_scores = assignment_service.final_score_output(assignment_id)
        print(user_scores)
        for ele in user_scores:
            if "id" not in ele:
                continue
            if ele["points_possible"] > 0:
                grade = ele["points_earned"] / ele["points_possible"] * 100
                if grade < 60:
                    result = 2
                elif grade < 80:
                    result = 1
                else:
                    result = 0

                matching_submissions.append(
                    {
                        "id": ele["id"],
                        "user": ele["username"],
                        "date": millisecond_to_date(msecond),
                        "result": result,
                        "assignment_id": assignment_id,
                        "score": grade,
                    }
                )
    else:
        # generate random submission
        for assignment in fake_assignment_data:
            if assignment["id"] == assignment_id:
                for i in range(0, len(fake_user_name)):
                    msecond = random_date(assignment["start"], assignment["end"])
                    matching_submissions.append(
                        {
                            "id": i,
                            "user": fake_user_name[i],
                            "date": millisecond_to_date(msecond),
                            "result": random_number_between(0, 2),
                            "assignment_id": assignment_id,
                        }
                    )

    response = jsonify(matching_submissions)
    return response
