from flask import Blueprint, jsonify, request
from api.service.userService import UserService

user_api = Blueprint("user_api", __name__)
userService = UserService()

# http://127.0.0.1:5000/v1/api/users/{id}
@user_api.route("/<int:user_id>", methods=["GET"])
def get_user_by_id(user_id):
    user = userService.get_user_by_id(user_id)
    if user:
        return jsonify(user)
    else:
        return jsonify({"error": "User not found"}), 404


# http://127.0.0.1:5000/v1/api/users?course_id=course-v1:VT%2BCS1234%2B2023Spring
@user_api.route("", methods=["GET"])
def get_users_by_course_id():
    course_id = request.args.get("course_id")
    users = userService.get_users_by_course_id(course_id)
    if users:
        return jsonify(users)
    else:
        return jsonify({"error": "User not found"}), 404
