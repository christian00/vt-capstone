from db.mongodb_connector import mongo_connector
from db.mysql_connector import mysql_connector
from bson import ObjectId


class AssignmentService:
    def __init__(self):
        self.course_collection = mongo_connector.getCollection(
            "modulestore.active_versions"
        )
        self.str_collection = mongo_connector.getCollection("modulestore.structures")
        self.def_collection = mongo_connector.getCollection("modulestore.definitions")
        self.connection = mysql_connector.get_connection()

    def find_assignments_by_course(self, mongo_course_id: str):
        assignments = []

        course = self.course_collection.find_one({"_id": ObjectId(mongo_course_id)})
        if course:
            draft_structure = self.str_collection.find_one(
                {"_id": course["versions"]["draft-branch"]}
            )
            if draft_structure:
                for block in draft_structure["blocks"]:
                    if block["block_type"] == "openassessment":
                        title = block["fields"]["display_name"]
                        assignments.append(
                            {
                                "id": str(
                                    block["block_id"]
                                ),  # id in modulestore.structures
                                "name": title,
                                "value": title.lower().replace(" ", "-"),
                                "start": block["fields"]["submission_start"],
                                "end": block["fields"]["submission_due"],
                                "mongo_course_id": str(
                                    course["_id"]
                                ),  # id in 'modulestore.active_versions
                            }
                        )
        return assignments

    def find_assignment_by_id(self, mongo_course_id: str, assignment_block_id: str):
        assignment = {
            "start": "2023-01-01T00:00:00+00:00",
            "end": "2023-12-01T00:00:00+00:00",
        }

        course = self.course_collection.find_one({"_id": ObjectId(mongo_course_id)})
        if course:
            draft_structure = self.str_collection.find_one(
                {"_id": course["versions"]["draft-branch"]}
            )
            if draft_structure:
                for block in draft_structure["blocks"]:
                    if (
                        block["block_type"] == "openassessment"
                        and str(block["block_id"]) == assignment_block_id
                    ):
                        assignment["start"] = block["fields"]["submission_start"]
                        assignment["end"] = block["fields"]["submission_due"]
                        break
        return assignment

    def find_sub_uuid_from_assignment_id(self, assignment_id):
        try:
            with self.connection.cursor() as cursor:
                cursor = self.connection.cursor()
                # first set the assignemnt id to have percent sign so that it can look at what block_v1 ends with

                # query for submission id
                subid_query = """
        SELECT submission_uuid
        FROM assessment_staffworkflow
        WHERE item_id LIKE %s;
        """
                new_id = "%" + assignment_id
                cursor.execute(subid_query, (new_id,))
                s_ids = cursor.fetchall()
                # remove the - in the id
                # rem_dash = s_id['submission_uuid'].replace("-", "")
                # return the submission_uuid
                return s_ids
        except Exception as e:
            print(f"Error querying MySQL database: {e}")
            return None

    # find the id from the submission uuid found in the submission
    def find_ids_from_sub_uuid(self, sub_uuid):
        try:
            with self.connection.cursor() as cursor:
                cursor = self.connection.cursor()
                ass_id_query = """
        SELECT id
        FROM submissions_submission
        WHERE uuid = (%s);
        """
                cursor.execute(ass_id_query, (sub_uuid,))
                ass_id = cursor.fetchall()
                return ass_id
        except Exception as e:
            print(f"Error querying MySQL database: {e}")
            return None

    # finding the final score from the assignment id number
    def find_score_from_sub_id(self, ass_id):
        try:
            with self.connection.cursor() as cursor:
                cursor = self.connection.cursor()
                score_query = """
        SELECT sc.student_item_id, si.student_id, sc.points_earned , sc.points_possible
        FROM submissions_score AS sc 
        JOIN submissions_studentitem AS si ON sc.student_item_id = si.id
        WHERE sc.id = %s;
        """
                cursor.execute(score_query, (ass_id["id"],))
                score_with_item_id = cursor.fetchone()
                return score_with_item_id
        except Exception as e:
            print(f"Error querying MySQL database: {e}")
            return None

    def find_user_by_anonymous_user_id(self, a_user_id):
        try:
            with self.connection.cursor() as cursor:
                cursor = self.connection.cursor()
                query = """
        SELECT au.id, au.username, sa.anonymous_user_id
        FROM student_anonymoususerid AS sa 
        JOIN auth_user AS au ON sa.user_id = au.id
        WHERE sa.anonymous_user_id = %s;
        """
                cursor.execute(query, (a_user_id,))
                user = cursor.fetchone()
                return user
        except Exception as e:
            print(f"Error querying MySQL database: {e}")
            return None

    # runs all queries and finds the final score for the assignment
    def final_score_output(self, assignment_id):
        sub_uuids = self.find_sub_uuid_from_assignment_id(assignment_id)
        return_scores = []
        for i in range(len(sub_uuids)):
            sub_uuids[i] = sub_uuids[i]["submission_uuid"].replace("-", "")
            sub_ids = self.find_ids_from_sub_uuid(sub_uuids[i])
            scores_with_item_id = []
            for sub_id in sub_ids:
                res = self.find_score_from_sub_id(sub_id)
                if res is None:
                    continue
                scores_with_item_id.append(res)
            for i in range(len(scores_with_item_id)):
                ele = scores_with_item_id[i]
                user = self.find_user_by_anonymous_user_id(ele["student_id"])
                if user is None:
                    continue
                scores_with_item_id[i]["id"] = user["id"]
                scores_with_item_id[i]["username"] = user["username"]
            return_scores.extend(scores_with_item_id)
        return return_scores
