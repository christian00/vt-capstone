from db.mysql_connector import mysql_connector
from db.mongodb_connector import mongo_connector
from bson import ObjectId

class CourseService:
    def __init__(self):
        self.connection = mysql_connector.get_connection()
        self.course_collection = mongo_connector.getCollection('modulestore.active_versions')
        self.str_collection= mongo_connector.getCollection('modulestore.structures')

    def get_courses_by_email(self, user_email):
        try:
            with self.connection.cursor() as cursor:
                cursor = self.connection.cursor()
                query = """
                    SELECT DISTINCT c.id, c.display_name, CONCAT(au.first_name, ' ', au.last_name) AS instructor, au.email, c.start, c.end, r.user_id
                    FROM student_courseaccessrole AS r
                    JOIN course_overviews_courseoverview AS c ON r.course_id = c.id
                    JOIN auth_user AS au ON au.id = r.user_id
                    WHERE au.email = (%s);
                """
                cursor.execute(query, (user_email,))
                result = cursor.fetchall()
                course_data = []
                for row in result:
                    # process value
                    value = row["display_name"].lower().replace(" ","-") if(row["display_name"]) else "course"

                    # find corresponding mongo course id 
                    splitId = str(row["id"]).split("+") if(row["id"]) else []
                    course_val = splitId[1] if len(splitId) > 1 else ""
                    run_val = splitId[2] if len(splitId) > 2 else ""
                    mongo_course = self.course_collection.find_one({'course': course_val, 'run': run_val})

                    course_data.append({
                        "mysql_id": row["id"] if(row["id"]) else None,
                        "mongo_id": str(mongo_course['_id']),
                        "name": row["display_name"] if(row["display_name"]) else None,
                        "value": value,
                        "start": row["start"].isoformat() if(row["start"]) else None,
                        "end": row["end"].isoformat() if(row["end"]) else None,
                    })
                return course_data
        except Exception as e:
            print(f"Error querying MySQL database: {e}")
            return None

    def get_courses_by_name(self, user_full_name):
        try:
            with self.connection.cursor() as cursor:
                cursor = self.connection.cursor()
                query = """
                    SELECT DISTINCT c.id, c.display_name, CONCAT(au.first_name, ' ', au.last_name) AS instructor, au.email, c.start, c.end, r.user_id
                    FROM student_courseaccessrole AS r
                    JOIN course_overviews_courseoverview AS c ON r.course_id = c.id
                    JOIN auth_user AS au ON au.id = r.user_id
                    WHERE (SELECT CONCAT(first_name, ' ', last_name)) = (%s);
                """
                cursor.execute(query, (user_full_name,))
                result = cursor.fetchall()
                course_data = []
                for row in result:
                    # process value
                    value = row["display_name"].lower().replace(" ","-") if(row["display_name"]) else "course"

                    # find corresponding mongo course id 
                    splitId = str(row["id"]).split("+") if(row["id"]) else []
                    course_val = splitId[1] if len(splitId) > 1 else ""
                    run_val = splitId[2] if len(splitId) > 2 else ""
                    mongo_course = self.course_collection.find_one({'course': course_val, 'run': run_val})

                    course_data.append({
                        "mysql_id": row["id"] if(row["id"]) else None,
                        "mongo_id": str(mongo_course['_id']),
                        "name": row["display_name"] if(row["display_name"]) else None,
                        "value": value,
                        "start": row["start"].isoformat() if(row["start"]) else None,
                        "end": row["end"].isoformat() if(row["end"]) else None,
                    })
                cursor.close()
                return { 'name': user_full_name, 'courses': course_data}
        except Exception as e:
            print(f"Error querying MySQL database: {e}")
            return None

    def get_course_by_mongo_id(self, mongo_course_id: str):
        course = self.course_collection.find_one({'_id': ObjectId(mongo_course_id)})
        course_block = {
            "start": '2023-02-01 00:00:00',
            "end": '2023-05-01 00:00:00',
        }

        if(course):
            draft_structure = self.str_collection.find_one({'_id': course['versions']['draft-branch']})
            if(draft_structure):
                for block in draft_structure['blocks']:
                    if(block['block_type'] == "course"):
                        if(block['fields'].get('start')):
                            course_block["start"] = str(block['fields']['start'])
                        if(block['fields'].get('end')):
                            course_block["end"] = str(block['fields']['end'])
                        break
        return course_block
