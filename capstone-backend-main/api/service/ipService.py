import re
import json

class Session:
    username = ''
    ip_address = ''
    timestamp = ''
    course_id = ''

    def __iter__(self):
        return iter([self.username, self.ip_address,
                     self.timestamp, self.course_id])

def get_course_ip_data():
    file = open("log/data/lms-tracking.log", "r")

    # ips for logged in users inside a course only
    data = []
    for line in file:
        record = json.loads(re.search('({.+)', line)[0])
        if record["username"] != '' and record["context"]["course_id"] != '':
            data.append(record)

    # one record per user per course
    unique_sessions = []
    for record in data:
        if any(x.username == record["username"]
               and x.course_id == record["context"]["course_id"]
               for x in unique_sessions) is False:
            s = Session()
            s.username = record["username"]
            s.ip_address = record["ip"]
            s.timestamp = record["time"]
            s.course_id = record["context"]["course_id"]
            unique_sessions.append(s)
        else:
            # get latest timestamp per user per course
            s = next(x for x in unique_sessions
                     if x.username == record["username"]
                     and x.course_id == record["context"]["course_id"])
            s.timestamp = record["time"]

    # convert objects to dictionary for JSON output
    sessions_dict_list = []
    for session in unique_sessions:
        session_dict = {"user": session.username,
                        "ip": session.ip_address,
                        "timestamp": session.timestamp,
                        "course_id": session.course_id}
        sessions_dict_list.append(session_dict)

    return sessions_dict_list

def get_course_ip_data_by_course(course_id):
    file = open("log/data/lms-tracking.log", "r")

    # ips for logged in users inside specified course only
    data = []
    for line in file:
        record = json.loads(re.search('({.+)', line)[0])
        if record["username"] != '' and record["context"]["course_id"] == course_id:
            data.append(record)

    # one record per user per course
    unique_sessions = []
    for record in data:
        if any(x.username == record["username"]
               and x.course_id == record["context"]["course_id"]
               for x in unique_sessions) is False:
            s = Session()
            s.username = record["username"]
            s.ip_address = record["ip"]
            s.timestamp = record["time"]
            s.course_id = record["context"]["course_id"]
            unique_sessions.append(s)
        else:
            # get latest timestamp per user per course
            s = next(x for x in unique_sessions
                     if x.username == record["username"]
                     and x.course_id == record["context"]["course_id"])
            s.timestamp = record["time"]

    # convert objects to dictionary for JSON output
    sessions_dict_list = []
    for session in unique_sessions:
        session_dict = {"user": session.username,
                        "ip": session.ip_address,
                        "timestamp": session.timestamp,
                        "course_id": session.course_id}
        sessions_dict_list.append(session_dict)
    return sessions_dict_list
