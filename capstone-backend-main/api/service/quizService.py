from db.mongodb_connector import mongo_connector
from db.mysql_connector import mysql_connector
from bson import ObjectId

class QuizService:
  def __init__(self):
    self.course_collection = mongo_connector.getCollection('modulestore.active_versions')
    self.str_collection= mongo_connector.getCollection('modulestore.structures')
    self.def_collection = mongo_connector.getCollection('modulestore.definitions')
    self.connection = mysql_connector.get_connection()

  def find_quizzes_by_course(self, mongo_course_id: str):
    quizzes = []

    course = self.course_collection.find_one({'_id': ObjectId(mongo_course_id)})
    if(course):
      draft_structure = self.str_collection.find_one({'_id': course['versions']['draft-branch']})
      if(draft_structure):
        for block in draft_structure['blocks']:
          if(block['block_type'] == "problem"):
            title = block['fields']['display_name']  
            start = None
            due = None
            
            #print('problem block id', str(block['block_id']))
            parent_vertical_block_id = self.find_parent_vertical_block_id(draft_structure, block['block_id'])
            #print('parent_vertical_block_id', parent_vertical_block_id)
            
            if(parent_vertical_block_id):
              parent_sequential_block_obj = self.find_parent_sequential_block_obj(draft_structure, parent_vertical_block_id)
            if(parent_sequential_block_obj):
              #print('parent', parent_sequential_block_obj)
              if(parent_sequential_block_obj['fields'].get('start')):
                start = parent_sequential_block_obj['fields']['start']
              if(parent_sequential_block_obj['fields'].get('due')):
                due = parent_sequential_block_obj['fields']['due']

            quizzes.append({
              "id": str(block['block_id']), # id in modulestore.structures
              "name": title,
              "value": title.lower().replace(" ","-"),
              "start": start, # start can only be set in subsection level
              "end": due, # due can only be set in subsection level
              "limit": 120, #count from start and due
              "mongo_course_id": str(course['_id']), # id in 'modulestore.active_versions
            })
    return quizzes

  def find_parent_vertical_block_id(self, draft_structure: any, problem_block_id: any):
    # find unit by problem id
    for block in draft_structure['blocks']:
      if(block['block_type'] == "vertical"):
        if(block['fields'].get('children')):
          for child in block['fields']['children']:
            if child[1]==str(problem_block_id):
              #print('found prob child', child)
              return block['block_id']
    return None

  def find_parent_sequential_block_obj(self, draft_structure: any, unit_id: any):
    # find sub-section by unit id
    for block in draft_structure['blocks']:
      if(block['block_type'] == "sequential"):
        if(block['fields'].get('children')):
          for child in block['fields']['children']:
            if child[1] == str(unit_id):
              #print('found vertical child', child)
              return block
    return None
  
  # From the quiz id that is passes into the system from MongoDB
  # the hash can be found in the MySQL data from the identifing id
  def find_hash_from_quiz_id(self, quiz_id):
    try:
      with self.connection.cursor() as cursor:
        cursor = self.connection.cursor()
        grade_query = """
        SELECT hashed
        FROM grades_visibleblocks
        WHERE JSON_UNQUOTE(JSON_EXTRACT(blocks_json, '$.blocks[0].locator')) LIKE %s
        """
        newid = '%' + quiz_id
        cursor.execute(grade_query, (newid,))
        hash = cursor.fetchone()
        return hash
    except Exception as e:
      print(f"Error querying MySQL database: {e}")
      return None
    
  # Query to get a list of the grades, name, and id of every student that has taken the quiz
  def find_grade_and_name(self, hash):
    try:
      with self.connection.cursor() as cursor:
        cursor = self.connection.cursor()
        grade_name_query = """
        SELECT au.username, ps.earned_all, ps.possible_all, ps.user_id
        FROM grades_persistentsubsectiongrade AS ps
        JOIN auth_user AS au ON au.id = ps.user_id
        WHERE ps.visible_blocks_hash = %s
        """
        cursor.execute(grade_name_query, (hash['hashed'],))
        gnlist = cursor.fetchall()
        return gnlist
    except Exception as e:
      print(f"Error querying MySQL database: {e}")
      return None
    
  # Runs the two queries so that you can find the results for the quiz that is requested
  def find_quiz_data(self, quiz_id):
    hash = self.find_hash_from_quiz_id(quiz_id)
    name_grade_list = self.find_grade_and_name(hash)
    return name_grade_list