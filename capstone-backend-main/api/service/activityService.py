import re
import json
import csv
from datetime import datetime

csv_session_file = 'log/session_log.csv'
csv_cum_file = 'log/cumulative_activity.csv'
log_path = "log/data/lms-tracking.log"

# define fields to retrieve
class Session:
    session_name = ''
    course_id = ''
    username = ''
    session_start = ''
    session_end = ''
    duration = ''

    def __iter__(self):
        return iter([self.session_name, self.course_id, self.username, self.session_start,
                     self.session_end, self.duration])

class Student:
    username = ''
    course_id = ''
    total_duration = ''

    def __iter__(self):
        return iter([self.username, self.course_id, self.total_duration])

def csv_to_json(csvFilePath):
    jsonArray = []

    # read csv file
    with open(csvFilePath, encoding='utf-8') as csvf:
        # load csv file data using csv library's dictionary reader
        csvReader = csv.DictReader(csvf)

        # convert each csv row into python dict
        for row in csvReader:
            # add this python dict to json array
            jsonArray.append(row)

    return jsonArray

def get_user_sessions():
    unique_sessions = []
    unique_users = []
    unique_students = []
    unique_courses = []
    try:
        # read records from csv
        session_csv = open(csv_session_file, 'r', newline='\n')
        reader = csv.reader(session_csv)
        next(reader, None)
        for record in reader:
            s = Session()
            s.session_name = record[0]
            s.course_id = record[1]
            s.username = record[2]
            s.session_start = datetime.strptime(record[3], '%Y-%m-%d %H:%M:%S.%f')
            s.session_end = datetime.strptime(record[4], '%Y-%m-%d %H:%M:%S.%f')
            s.duration = float(record[5])

            # add unique elements to arrays
            unique_sessions.append(s)
            if unique_sessions.count(s) == 0:
                unique_sessions.append(s)
            if unique_users.count(s.username) == 0:
                unique_users.append(s.username)
            if unique_courses.count(s.course_id) == 0:
                unique_courses.append(s.course_id)

        session_csv.close()
    except FileNotFoundError:
        print("No current log file found")

    file = open(log_path, "r")

    # logs for logged in users only
    data = []
    for line in file:
        record = json.loads(re.search('({.+)', line)[0])
        if record["username"] != '':
            data.append(record)
    file.close()

    # TODO: connect to openedx log location

    # one record per session per class
    for record in data:
        context = record['context']
        if context['course_id'] != '':
            if any(x.session_name == record["session"] and x.course_id == context['course_id']
                   for x in unique_sessions) is False:
                s = Session()
                s.session_name = record["session"]
                s.course_id = context['course_id']
                s.username = record["username"]
                time = datetime.strptime(record['time'], '%Y-%m-%dT%H:%M:%S.%f+00:00')
                s.session_start = time
                s.session_end = time
                s.duration = 0
                unique_sessions.append(s)
                if unique_users.count(s.username) == 0:
                    unique_users.append(s.username)
                if unique_courses.count(s.course_id) == 0:
                    unique_courses.append(s.course_id)
            else:
                # update session end time w/ latest record
                s = next(x for x in unique_sessions
                         if x.session_name == record["session"] and x.course_id == context['course_id'])
                s.session_end = datetime.strptime(record['time'], '%Y-%m-%dT%H:%M:%S.%f+00:00')
                s.duration = round((s.session_end-s.session_start).total_seconds()/60, 3)

    # write records to csv
    session_csv = open(csv_session_file, 'w', newline='\n')
    w = csv.writer(session_csv)
    w.writerow(['Session Name', 'Course id', 'Username', 'Session Start', 'Session End', 'Duration'])
    for session in unique_sessions:
        w.writerow(list(session))
    session_csv.close()

    # write records to cumulative csv
    session_csv = open('log/cumulative_activity.csv', 'w', newline='\n')
    w = csv.writer(session_csv)
    w.writerow(['Username', 'Course Id', 'Duration'])
    for name in unique_users:
        for course in unique_courses:
            duration = 0
            for session in unique_sessions:
                if session.username == name and session.course_id == course:
                    duration += float(session.duration)
            if duration != 0:
                s = Student()
                s.username = name
                s.course_id = course
                s.total_duration = duration
                unique_students.append(s)
                w.writerow(list(s))

    session_csv.close()

    sessions = csv_to_json(csv_session_file)
    students = csv_to_json(csv_cum_file)

    return sessions, students

def getUniqueSessions(course_id):
    sessions, _ = get_user_sessions()
    course_sessions = []
    for record in sessions:
        if record['Course id'] == course_id:
            course_sessions.append(record)
    return course_sessions

def getStudentSessions(course_id):
    _, students = get_user_sessions()
    course_students = []
    for record in students:
        if record['Course Id'] == course_id:
            course_students.append(record)
    return course_students

# getStudentSessions('course-v1:VT+Capstone101+2023Spring')

# all_sessions, all_students = get_user_sessions()
# print(all_students)
#
# print(all_sessions)



