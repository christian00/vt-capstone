from db.mysql_connector import mysql_connector

class UserService:
    def __init__(self):
        self.connection = mysql_connector.get_connection()

    def get_user_by_id(self, user_id):
        try:
            with self.connection.cursor() as cursor:
                cursor = self.connection.cursor()
                query = """
                    SELECT u.id, u.username, u.email, u.is_staff, up.name, up.year_of_birth, up.gender, up.level_of_education 
                    FROM auth_userprofile AS up
                    JOIN auth_user AS u ON up.user_id = u.id
                    WHERE u.id = (%s);
                """
                cursor.execute(query, (user_id,))
                user = cursor.fetchone()
                cursor.close()
            return user
        except Exception as e:
            print(f"Error querying MySQL database: {e}")
            return None
    
    def get_user_by_email(self, email):
        try:
            with self.connection.cursor() as cursor:
                cursor = self.connection.cursor()
                query = """
                    SELECT u.id, u.username, u.email
                    FROM auth_user AS u
                    WHERE u.email = (%s);
                """
                cursor.execute(query, (email,))
                user = cursor.fetchone()
                cursor.close()
            return user
        except Exception as e:
            print(f"Error querying MySQL database: {e}")
            return None

    def get_user_by_username(self, username):
        try:
            with self.connection.cursor() as cursor:
                cursor = self.connection.cursor()
                query = """
                    SELECT u.id, u.username, u.email
                    FROM auth_user AS u
                    WHERE u.username = (%s);
                """
                cursor.execute(query, (username,))
                user = cursor.fetchone()
                cursor.close()
            return user
        except Exception as e:
            print(f"Error querying MySQL database: {e}")
            return None

    def get_users_by_course_id(self, course_id):
        try:
            with self.connection.cursor() as cursor:
                cursor = self.connection.cursor()
                query = """
                    SELECT au.id, au.username, au.email
                    FROM student_courseenrollment AS sc
                    JOIN auth_user AS au ON sc.user_id = au.id
                    WHERE sc.course_id = (%s);
                """
                cursor.execute(query, (course_id,))
                result = cursor.fetchall()
                user_data = []
                for row in result:
                    user_data.append(
                        {
                            "id": row["id"],
                            "username": row["username"],
                            "email": row["email"]
                        }
                    )
                cursor.close()
            return user_data
        except Exception as e:
            print(f"Error querying MySQL database: {e}")
            return None
