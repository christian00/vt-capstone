fake_ml_data = [{
    'user': 'Alice',
    'result': 0,
    'course_id': '1'
  },{
    'user': 'Bob',
    'result': 1,
    'course_id': '1'
  },{
    'user': 'Charlie',
    'result': 2,
    'course_id': '1'
  },{
    'user': 'Dennis',
    'result': 0,
    'course_id': '1'
  },{
    'user': 'Eland', 
    'result': 1,
    'course_id': '1'
  },{
    'user': 'Frank', 
    'result': 1,
    'course_id': '2'
  },{
    'user': 'George', 
    'result': 2,
    'course_id': '1'
  },{
    'user': 'Holly', 
    'result': 2,
    'course_id': '1'
  },{
    'user': 'Ivan',
    'result': 0, 
    'course_id': '1'
  },{
    'user': 'Joe',
    'result': 0, 
    'course_id': '2'
  },{
    'user': 'Kelly', 
    'result': 0,
    'course_id': '2'
  },{
    'user': 'Leo', 
    'result': 0,
    'course_id': '2'
  },{
    'user': 'Morris', 
    'result': 1,
    'course_id': '1'
  },{
    'user': 'Nancy', 
    'result': 1,
    'course_id': '1'
  },{
    'user': 'Oreo', 
    'result': 1,
    'course_id': '2'
  },{
    'user': 'Patrick',
    'result': 1,
    'course_id': '1'
  },{
    'user': 'Quanzo', 
    'result': 2,
    'course_id': '1'
  },{
    'user': 'Robert', 
    'result': 2,
    'course_id': '1'
  },{
    'user': 'Sue', 
    'result': 2,
    'course_id': '2'
  },{
    'user': 'Terry',
    'result': 2,
    'course_id': '1'
  },{
    'user': 'Udemy',
    'result': 2,
    'course_id': '1'
  },{
    'user': 'Victoria',
    'result': 0,
    'course_id': '1'
  },{
    'user': 'Wendy',
    'result': 0,
    'course_id': '2'
  },{
    'user': 'Xen', 
    'result': 0,
    'course_id': '1'
  },
]