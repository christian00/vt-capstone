import time
import datetime

def millisecond_to_date(millisecond):
  date = datetime.datetime.fromtimestamp(millisecond/1000.0)
  date = date.strftime('%Y-%m-%d')
  return date

def millisecond_to_hour(millisecond):
  date = datetime.datetime.fromtimestamp(millisecond/1000.0)
  date = date.strftime('%Y-%m-%d %H')
  return date

def millisecond_to_minute(millisecond):
  date = datetime.datetime.fromtimestamp(millisecond/1000.0)
  date = date.strftime('%Y-%m-%d %H:%M')
  return date