fake_courses_data = [{
        'mysql_id': '1',
        'mongo_id': '1',
        'name': 'Intro to Programming',
        'instructor': 'John Smith',
        'email': 'hychien2000@gmail.com',
        'start': "2022-05-03T12:00:00.000Z",
        'end': "2022-09-30T23:59:59.000Z"
    },{
        'mysql_id': '2',
        'mongo_id': '2',
        'name': 'Web Development',
        'instructor': 'Jane Doe',
        'email': 'hychien2000@gmail.com',
        'start': "2022-05-03T12:00:00.000Z",
        'end': "2022-09-30T23:59:59.000Z"
    },{
        'mysql_id': '3',
        'mongo_id': '3',
        'name': 'Database Design',
        'instructor': 'John Smith',
        'email': 'janedoe@school.edu',
        'start': "2023-01-13T12:00:00.000Z",
        'end': "2023-05-13T23:59:59.000Z"
    },{
        'mysql_id': '4',
        'mongo_id': '4',
        'name': 'Mobile App Development',
        'instructor': 'Mark Johnson',
        'email': 'markjohnson@school.edu',
        'start': "2022-08-20T00:00:00.000Z",
        'end': "2023-01-10T23:59:59.000Z"
    },
]