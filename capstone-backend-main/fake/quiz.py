fake_quiz_data = [{
    'id': 1,
    'name': "Quiz1",
    'value': "quiz1",
    'start': "2022-06-10T12:00:00.000Z",
    'end': "2022-06-17T23:59:59.000Z",
    'limit': 60,
    'course_id': "1"
  },
  {
    'id': 2,
    'name': "Midterm",
    'value': "midterm",
    'start': "2022-07-10T14:00:00.000Z",
    'end': "2022-07-10T15:59:59.000Z",
    'limit': 120,
    'course_id': "1"
  },
  {
    'id': 3,
    'name': "Quiz2",
    'value': "quiz2",
    'start': "2022-08-10T12:00:00.000Z",
    'end': "2022-08-17T23:59:59.000Z",
    'limit': 60,
    'course_id': "1"
  },{
    'id': 4,
    'name': "Final",
    'value': "final",
    'start': "2022-09-20T17:00:00.000Z",
    'end': "2022-09-20T18:59:59.000Z",
    'limit': 120,
    'course_id': "1"
  },
  {
    'id': 5,
    'name': "Quiz1",
    'value': "quiz1",
    'start': "2022-06-10T12:00:00.000Z",
    'end': "2022-06-17T23:59:59.000Z",
    'limit': 60,
    'course_id': "2"
  },
  {
    'id': 6,
    'name': "Midterm",
    'value': "midterm",
    'start': "2022-07-15T12:00:00.000Z",
    'end': "2022-07-17T23:59:59.000Z",
    'limit': 120,
    'course_id': "2"
  },
  {
    'id': 7,
    'name': "Final",
    'value': "final",
    'start': "2022-09-05T12:00:00.000Z",
    'end': "2022-09-10T23:59:59.000Z",
    'limit': 120,
    'course_id': "2"
  },
]

# duration units: mins
fake_duration_data = [{
    'id': 1,
    'user': 'Alice',
    'duration': "35",
    'quiz_id': 1
  },{
    'id': 2,
    'user': 'Alice',
    'duration': "80",
    'quiz_id': 2
  },{
    'id': 3,
    'user': 'Alice',
    'duration': "54",
    'quiz_id': 3
  },{
    'id': 4,
    'user': 'Alice',
    'duration': "110",
    'quiz_id': 4
  },{
    'id': 5,
    'user': 'Alice',
    'duration': "23",
    'quiz_id': 5
  },{
    'id': 6,
    'user': 'Alice',
    'duration': "60",
    'quiz_id': 6
  },{
    'id': 7,
    'user': 'Alice',
    'duration': "105",
    'quiz_id': 7
  },{
    'id': 8,
    'user': 'Bob',
    'duration': "32",
    'quiz_id': 1
  },{
    'id': 9,
    'user': 'Bob',
    'duration': "57",
    'quiz_id': 2
  },{
    'id': 10,
    'user': 'Bob',
    'duration': "48",
    'quiz_id': 3
  },{
    'id': 11,
    'user': 'Bob',
    'duration': "116",
    'quiz_id': 4
  },{
    'id': 12,
    'user': 'Bob',
    'duration': "19",
    'quiz_id': 5
  },{
    'id': 13,
    'user': 'Bob',
    'duration': "89",
    'quiz_id': 6
  },{
    'id': 14,
    'user': 'Bob',
    'duration': "76",
    'quiz_id': 7
  },
]