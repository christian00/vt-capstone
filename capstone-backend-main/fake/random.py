import random
import math
import time

# list of[date to milliseconds , accumulated login times]
    
def generate_random_time(start, end, time_format, prop):
    """Get a time at a proportion of a range of two formatted times.

    start and end should be strings specifying times formatted in the
    given format (strftime-style), giving an interval [start, end].
    prop specifies how a proportion of the interval to be taken after
    start.  The returned time will be in the specified format.
    """
    stime = time.mktime(time.strptime(start, time_format))
    etime = time.mktime(time.strptime(end, time_format))
    ptime = stime + prop * (etime - stime)

    # return time.strftime(time_format, time.localtime(ptime)) # same formate
    return round(ptime*1000) #miliseconds

def random_date(start, end, time_format='%Y-%m-%dT%H:%M:%S.%fZ'):
    return generate_random_time(start, end, time_format, random.random())

def random_duration(limit):
    randomDuration =  random.uniform(0.3, 0.95) * limit
    return int(randomDuration)

def random_activity_count(min=1, max=60):
    return random.randint(min, max)

def random_number_between(min, max):
    return random.randint(min, max)
