from db.mysql_connector import MySQLConnector

fake_assignment_data = [
    {
        "id": "1",
        "name": "Assignment1",
        "value": "assignment1",
        "start": "2022-05-10T12:00:00.000Z",
        "end": "2022-05-17T23:59:59.000Z",
        "course_id": "1"
    },
    {
        "id": "2",
        "name": "Assignment2",
        "value": "assignment2",
        "start": "2022-05-24T12:00:00.000Z",
        "end": "2022-05-31T23:59:59.000Z",
        "course_id": "1"
    },
    {
        "id": "3",
        "name": "Project Proposal",
        "value": "project proposal",
        "start": "2022-06-01T12:00:00.000Z",
        "end": "2022-06-14T23:59:59.000Z",
        "course_id": "1"
    },
    {
        "id": "4",
        "name": "Assignment3",
        "value": "assignment3",
        "start": "2022-07-01T12:00:00.000Z",
        "end": "2022-07-14T23:59:59.000Z",
        "course_id": "1"
    },
    {
        "id": "5",
        "name": "Assignment4",
        "value": "assignment4",
        "start": "2022-07-21T12:00:00.000Z",
        "end": "2022-07-30T23:59:59.000Z",
        "course_id": "1"
    },
    {
        "id": "6",
        "name": "Assignment5",
        "value": "assignment5",
        "start": "2022-08-07T12:00:00.000Z",
        "end": "2022-08-20T23:59:59.000Z",
        "course_id": "1"
    },
    {
        "id": "7",
        "name": "Project Report",
        "value": "project report",
        "start": "2022-09-14T12:00:00.000Z",
        "end": "2022-09-20T23:59:59.000Z",
        "course_id": "1"
    },
]

fake_submission_data = [
    {"id": "1", "user": "Alice", "date": "2022-05-12T14:59:59.000Z", "assignment_id": 1},
    {"id": "2", "user": "Alice", "date": "2022-05-30T23:59:59.000Z", "assignment_id": 2},
    {"id": "3", "user": "Alice", "date": "2022-06-10T23:59:59.000Z", "assignment_id": 3},
    {"id": "4", "user": "Alice", "date": "2022-07-10T23:59:59.000Z", "assignment_id": 4},
    {"id": "5", "user": "Alice", "date": "2022-07-25T23:59:59.000Z", "assignment_id": 5},
    {"id": "6", "user": "Alice", "date": "2022-08-10T23:59:59.000Z", "assignment_id": 6},
    {"id": "7", "user": "Alice", "date": "2022-09-15T23:59:59.000Z", "assignment_id": 7},
    {"id": "8", "user": "Bob", "date": "2022-05-14T09:59:59.000Z", "assignment_id": 1},
    {"id": "9", "user": "Bob", "date": "2022-05-28T23:59:59.000Z", "assignment_id": 2},
    {"id": "10", "user": "Bob", "date": "2022-06-12T23:59:59.000Z", "assignment_id": 3},
    {"id": "11", "user": "Bob", "date": "2022-07-09T23:59:59.000Z", "assignment_id": 4},
    {"id": "12", "user": "Bob", "date": "2022-07-28T23:59:59.000Z", "assignment_id": 5},
    {"id": "13", "user": "Bob", "date": "2022-08-08T23:59:59.000Z", "assignment_id": 6},
    {"id": "14", "user": "Bob", "date": "2022-09-19T23:59:59.000Z", "assignment_id": 7},
]

"""
def get_submission_score_data():
    mysql_connector = MySQLConnector()
    connection = mysql_connector.get_connection()
    try:
        with connection.cursor() as cursor:
            # Query to Fetch Data from score Table
            query = "SELECT * FROM submissions_score"
            cursor.execute(query)
            result = cursor.fetchall()
            submission_data = []
            for row in result:
                submission_data.append(
                    {
                        "id": row["id"],
                        "user": row["student_item_id"],
                        "date": row["created_at"].isoformat(),
                        "assignment_id": row["submission_id"],
                    }
                )
        return submission_data
    finally:
        connection.close()

submission_data = get_submission_score_data()
"""