from flask import Flask, jsonify, request
from flask_cors import CORS
import config
from api.courseApi import course_api
from api.activityApi import activity_api
from api.assignmentApi import assignment_api
from api.quizApi import quiz_api
from api.userApi import user_api

app = Flask(__name__)
CORS(app, cors_allowed_origins=config.frontend_local) # CORS(app, resources={r"/v1/api/*": {"origins": "*"}})
app.config.from_object(config)

V1_API = '/v1/api'
app.register_blueprint(course_api, url_prefix= V1_API+'/courses')
app.register_blueprint(activity_api, url_prefix= V1_API+'/activities')
app.register_blueprint(assignment_api, url_prefix= V1_API+'/assignments')
app.register_blueprint(quiz_api, url_prefix= V1_API+'/quizzes')
app.register_blueprint(user_api, url_prefix= V1_API+'/users')

@app.route("/")
def hello_world():
    return "<p>Hello, World! 2023 VT capstone</p>"

@app.route("/test")
def hello_world2():
    return "<p>Hello, World! 2023 VT capstone from gitlab</p>"

if __name__ == '__main__':
    app.run(debug=True)