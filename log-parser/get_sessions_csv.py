import re
import json
import csv


class Session:
    session_name = ''
    username = ''
    session_start = ''
    session_end = ''

    def __iter__(self):
        return iter([self.session_name, self.username, self.session_start,
                     self.session_end])


file = open("log-data/tracking.log", "r")
data = []

for line in file:
    record = json.loads(re.search('({.+)', line)[0])
    if record["username"] != '':
        data.append(record)


unique_sessions = []

for record in data:
    if any(x.session_name == record["session"]
           for x in unique_sessions) is False:
        s = Session()
        s.session_name = record["session"]
        s.username = record["username"]
        s.session_start = record["time"]
        s.session_end = record["time"]
        unique_sessions.append(s)
    else:
        s = next(x for x in unique_sessions
                 if x.session_name == record["session"])
        s.session_end = record["time"]


session_csv = open('session_log.csv', 'w', newline='\n')
w = csv.writer(session_csv)
w.writerow(['Session Name', 'Username', 'Session Start', 'Session End'])
for session in unique_sessions:
    w.writerow(list(session))
session_csv.close()
