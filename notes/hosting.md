## Hosting Open edX platform

### Local machine 
1. Run local_setup.sh, which will automatically do following:
    * create a virtual environment, and install necessary packages, including tutor
    * run tutor local launch
    * run add_dns_records.sh, which handles HOST name setting

2. Now you can access LMS with url: `local.overhang.io`, CMS with url `studio.local.overhang.io`

3. Creat admin accounts so that you can create courses and course content
    * [Official tutorial](https://docs.tutor.overhang.io/local.html#creating-a-new-user-with-staff-and-admin-rights)
    * remember that all tutor commands should be operated in the virtual environment

### VT CS cloud
1. create your account [here](https://admin.cs.vt.edu/create.pl)
    * [account policy](https://wiki.cs.vt.edu/index.php/User_Account_Policy)
2. [login portal](https://cloud.cs.vt.edu/login)
    * [quick start guide](https://wiki.cs.vt.edu/index.php/Cloud_Quickstart)

3. Set configuration: update k8s config on your local
    1. [install kubectl (CLI for k8s)](https://kubernetes.io/docs/tasks/tools/)
    2. Go to [Cloud management GUI](https://cloud.cs.vt.edu/)  
    3. Go to Discovery -> Cluster -> Kubeconfig File-> copy and paste to ~/.kube/config on your local machine
        * if there's no such file and directory on your local, just create one
    4. Modify the config file you just created/updated
        * enter command `kubectl config set-context --current --namespace=openedx`
        * now, your config file should have been modified, use command `cat ~/.kube/config` to check whether namespace openedx is specified
    5. Now, you should be able to access containers on Cloud via CLI from local
        * try command `kubectl get pod -v6`, which will show all pods

4. Show files on cloud
* `kubectl exec [pod name] -- ls -la /[directory path]`
* `kubectl exec [pod name] -- cat /[file path]`

## Reference
[tutor developer forum](https://discuss.overhang.io/)
[Open edX forum](https://discuss.openedx.org/)