## Fetching data 

### Fetching cloud logs
  1. open a terminal on your local 
  2. find the pod name on Cloud
      * enter `LMS_POD_NAME=$(kubectl get pods -l app.kubernetes.io/name=lms -o jsonpath="{.items[0].metadata.name}")`
      * or you can show all pods using `kubectl get pod -v6`
  3. copy the logs to your local
      * `kubectl cp openedx/$LMS_POD_NAME:/openedx/data/logs/tracking.log tracking.log`

### Fetching cloud database on local machine

#### MySQL 
  1. open a terminal on your local 
  2. forward cloud DB to your local
      * enter `kubectl port-forward mysql-b4d677bdd-lfrqm 3306:3306`
      * or `kubectl port-forward mysql-b4d677bdd-lfrqm [any local port]:3306` if port binding failed
      * find which process use the port: use `lsof -i :[port number]` to check pid and kill process if needed
      * remember to update the port number in `.env` in backend project if not using default port `3306`
  3. run MySQL CLI/GUI locally

#### MongoDB
  1. open a terminal on your local 
  2. forward cloud DB to your local
      * enter `kubectl port-forward mongodb-55d8d5bb-k9l88 27017:27017`
      * or `kubectl port-forward mongodb-55d8d5bb-k9l88 [any local port]:27017` if port binding failed
      * find which process use the port: use `lsof -i :[port number]` to check pid and kill process if needed
      * remember to update the port number in `.env` in backend project if not using default port `27017`
  3. run MongoDB CLI/GUI locally

#### MongoDB Collections
* modulestore.active_versions
  * course data, each row is a course
  * CMS, studio ver., instructor view is specified as `draft-branch`
    * now, we are using this version for data visulization
  * LMS, student view is specified as `published-branch`

* modulestore.structures
  * all sanpshots/versions of course-branch, each row is a snapshot
    *  when a course author changes a course, or a block in the course, a new course structure is saved; the previous course structure, and previous versions of blocks within the structure, remain in the database and are not modified. [see docs](https://docs.huihoo.com/edx/open-edx-developer-guide/modulestores/split-mongo.html#course-structures)
  * can find latest structure using `draft-branch` id in modulestore.active_versions
  * all courses content(block, ex. course, section(chapter), sub-section(sequntical), unit(vertical), component(problem, assessment,.. etc)) are stored in an array in each snapshot, no nested/leveled strcuture

* modulestore.definitions
  * details of each block
  * can find corresponding definition using `definition` id in modulestore.structures