## Customize Open edX platform
### if using tutor
#### [Build customized edx-platform image](https://docs.tutor.overhang.io/configuration.html#custom-open-edx-docker-image)
1. fork [openedx/edx-platform](https://github.com/openedx/edx-platform) repository
    * pick [legitimate version](https://docs.tutor.overhang.io/configuration.html#running-a-fork-of-edx-platform) 
2. make customized changes
3. [build the customized image using tutor command](https://docs.tutor.overhang.io/configuration.html#running-a-different-openedx-docker-image)

## Customize Frontend of Open edX platform
three approaches:
1. Directly modify HTML/JS/CSS in edx-platform and build customized docker image 
2. Use MFE (Micro-Frontend)
    * [tutorial](https://openedx.atlassian.net/wiki/spaces/FEDX/pages/1265467645/Open+edX+and+Microfrontends)
3. [Use MFE plugin for tutor](https://github.com/overhangio/tutor-mfe)
