# Create courses on edX

## Content
### Outline
* List of Sections
    * Name
    * Release data, time
    * Visibility
    * List of Subsections
        * List of Units
            * Discussion
            * Library Content
            * Text
            * Open Response
            * Problem 
            * Video
### Updates
notify important dates, exams etc.
* List of Updates
### Pages & Resources
### Files & Uploads

## Settings
### Schedule & Details
* Basic information
* Course Schedule
    * Start Date, Time
    * End Date, Time
    * Enrollment start Date, Time
    * Enrollment end Date, Time
* Course Details
    * Language
    * Short Description
    * Overview (HTML)
### Grading
* Grade Range
* Grace Period on Deadline
* Assignment Types
    * Name
    * Abbreviation
    * Weight
    * Total Number
    * Number of Droppable
### Course Team
### Group Configurations
### Advanced Settings
### Certificates
should first [enable certificate](https://edx.readthedocs.io/projects/edx-installing-configuring-and-running/en/latest/configuration/enable_certificates.html)
    1. cloud.cs.vt.edu explorer
    1. ConfigMaps-> edit cms.env.yml & lms.env.yml
    
## Tools
### Import
### Export
### Checklists

# Canvas to edX
example: canvas Week2 to Week6 

* Strcutre/Term Comparison

| Canvas | edX       |          |
| -------| ----------|----------|
| Module | Section   ||
|        | Subsection||
| Item   | Unit      ||
|        | Components||
|Discussion||Discussion|
| ?       ||Libarary Content|
|Text Header/Page||Text|
|Assignment||Open Response|
|Quiz||Problem|
|Page||Video|

## Create contents manually
* Add Section
    * Add [Subsections](https://help.appsembler.com/article/88-how-grading-assignments-weights-work)
        * Add Units
            * Add Components
                * Discussion
                * Libarary Content
                * Text
                * [Open Response Assessment](https://help.appsembler.com/article/241-open-response-assessment-ora-basic-walkthrough)
                * Problem
                * Video

*Note: grading is configured at the subsection level.* 

## Create content automatically
canvas -> leftbar: settings -> rightbar: export course content -> choose 1. course(imscc file) or 2. quiz(zip file)
### course.imscc
* change imscc to zip and unzip it
    * Course structure information
        * course_seetings -> module_meta.xml
        * each week is declared as one <items></items>
            * identefier id corresponds to folder name 

* [Importing courses to edX](https://edx.readthedocs.io/projects/open-edx-building-and-running-a-course/en/latest/releasing_course/export_import_course.html)
    * The course that you import must be in a .tar.gz file
    * This .tar.gz file must contain a course.xml file in a course data directory.
        * follor OLX (open learning XML), the edX markup format [OLX guide here](https://edx.readthedocs.io/projects/edx-open-learning-xml/en/latest/index.html#edx-open-learning-xml-guide)
    * The tar.gz file must have the same name as the course data directory.

* Steps
    1. From the Tools menu, select Import.
    1. Select Choose a File to Import.
    1. Locate the file that you want, and then Select Open.
    1. Select Replace my course with the one above.

### quiz.zip
* XML file